const { fromWei, isBN } = require("web3-utils");

export function formatStatus(status) {
    let s = "";
    s += `\ntotal   : ${formatTimestamp(status.timestamp).padStart(22, " ")} | ${formatNumber(fromWei(status.totalAmount))} |`;
    s += `\n          ${"start timestamp".padStart(22, " ")} | ${"amount".padStart(29, " ")} | ${"age".padStart(10, " ")} | ${"current interest".padStart(29, " ")} | ${"current claimable".padStart(29, " ")} | ${"next".padStart(10, " ")} | ${"next timestamp".padStart(22, " ")} | ${"next claimable".padStart(29, " ")}`;
    for (let i = 0; i < status.stakes.length; i++) {
        let stake = status.stakes[i];
        let age = stake.timestamp == 0 ? 0 : Math.round((status.timestamp - stake.timestamp) / 3600 * 10) / 10;
        let next = stake.nextRebaseTimestamp == 0 ? 0 : Math.round((stake.nextRebaseTimestamp - status.timestamp) / 3600 * 10) / 10;
        s += `\nstake${(i + 1).toString().padStart(2, '0')} : ${formatTimestamp(stake.timestamp).padStart(22, " ")} | ${formatNumber(fromWei(stake.amount))} | ${age.toFixed(1).toString().padStart(10, ' ')} | ${formatNumber(fromWei(stake.interest))} | ${formatNumber(fromWei(stake.claimable))} | ${next.toFixed(1).toString().padStart(10, ' ')} | ${formatTimestamp(stake.nextRebaseTimestamp).padStart(22, " ")} | ${formatNumber(fromWei(stake.nextRebaseClaimable))}`;
    }
    return s;
}

export function formatNumber(s) {
    if (s.indexOf('.') == -1) s = s + '.';
    return s.substring(0, s.indexOf('.')).padStart(10, ' ') + s.substring(s.indexOf('.'), s.length).padEnd(19, '0');
}

export function formatDate(d) {
    return `${d.getUTCDate().toString().padStart(2, '0')}-${(d.getUTCMonth() + 1).toString().padStart(2, '0')}-${d.getUTCFullYear().toString().padStart(4, '0')} ${d.getUTCHours().toString().padStart(2, '0')}:${d.getUTCMinutes().toString().padStart(2, '0')}:${d.getUTCSeconds().toString().padStart(2, '0')}`;
}

export function formatTimestamp(t) {
    if (isBN(t)) t = t.toNumber();
    if (t == 0) { return '00-00-0000 00:00:00' };
    return formatDate(new Date(t * 1000));
}
