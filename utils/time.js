// Advances time by given amount of seconds
export async function advanceTime(duration) {
    const id = Date.now();
    return new Promise((resolve, reject) => {
        web3.currentProvider.send({
            jsonrpc: '2.0',
            method: 'evm_increaseTime',
            params: [duration],
            id: id,
        }, err1 => {
            if (err1) return reject(err1);
            web3.currentProvider.send({
                jsonrpc: '2.0',
                method: 'evm_mine',
                id: id + 1,
            }, (err2, res) => (err2 ? reject(err2) : resolve(res)));
        });
    });
}

// Advances to the next block
export async function advanceBlock() {
    return new Promise((resolve, reject) => {
        web3.currentProvider.send({
            jsonrpc: '2.0',
            method: 'evm_mine',
            id: Date.now(),
        }, (err, res) => (err ? reject(err) : resolve(res)));
    });
}

export async function advanceTimeAndBlock(duration) {
    await advanceTime(duration);
    await advanceBlock();
    return Promise.resolve(getDate());
}

// Returns the time of the last mined block in seconds
export async function now() {
    let now;
    const latestBlock = await web3.eth.getBlockNumber();
    await web3.eth.getBlock(latestBlock, function (error, result) {
        if (!error) {
            now = result.timestamp;
        }
        else {
            console.error(error);
        }
    })
    return now;
}

export async function getBlock() {
    return Promise.resolve(web3.eth.getBlock('latest'));
}

export async function getDate() {
    const block = await getBlock();
    return new Date(block.timestamp * 1000);
}

export const period = {
    seconds: function (t) { return t; },
    minutes: function (t) { return t * this.seconds(60) },
    hours: function (t) { return t * this.minutes(60) },
    days: function (t) { return t * this.hours(24) },
    weeks: function (t) { return t * this.days(7) },
    years: function (t) { return t * this.days(365) }
}
