const { toBN } = require("web3-utils");

export class Converter {

    constructor(price, decimals, rate) {
        this.price = price;
        this.decimals = decimals;
        this.rate = rate;
    }

    /**
     * @notice Conversion calculation from cash amount (i.e. native currency)
     * @param cashAmount Amount of cash to be converted
     * @return tokenAmount Amount of tokens that can be purchased with the specified cashAmount
     * @return usdValue USD equivalent of cashAmount
     */
    convertFromCashAmount = (cashAmount) => {
        return {
            tokenAmount: toBN(cashAmount).mul(toBN(this.price)).div(toBN(10 ** this.decimals)).div(toBN(this.rate)).mul(toBN(10 ** 4)),
            usdValue: toBN(cashAmount).mul(toBN(this.price)).div(toBN(10 ** this.decimals))
        };
    }

    /**
     * @notice Conversion calculation from token amount
     * @param tokenAmount Amount of tokens to be converted
     * @return cashAmount Amount of cash (i.e. native currency) needed to purchase the specified tokenAmount
     * @return usdValue USD equivalent of tokenAmount
     */
    convertFromTokenAmount = (tokenAmount) => {
        return {
            cashAmount: toBN(tokenAmount).div(toBN(this.price)).mul(toBN(10 ** this.decimals)).mul(toBN(this.rate)).div(toBN(10 ** 4)),
            usdValue: toBN(tokenAmount).mul(toBN(this.rate)).div(toBN(10 ** 4))
        };
    }

    /**
     * @notice Conversion calculation from USD value
     * @param usdValue Amount of USD to be converted
     * @return tokenAmount Amount of tokens that can be purchased with the specified usdValue
     * @return cashAmount Amount of cash (i.e. native currency) equivalent to the specified usdValue
     */
    convertFromUSDValue = (usdValue) => {
        return {
            tokenAmount: toBN(usdValue).div(toBN(this.rate)).mul(toBN(10 ** 4)),
            cashAmount: toBN(usdValue).div(toBN(this.price)).mul(toBN(10 ** this.decimals))
        };
    }
}
