# Abstract

This project consists of the following smart-contracts written in Solidity and targeted for EVM-based blockchains:

* *ExenoToken_Layer1* and *ExenoToken_Layer2* - instantiate an Exeno Coin (EXN) token on various blockchains, 
* *ExenoTokenIco* - facilitates the public sale of the token,
* *ExenoTokenStaking* - implements a staking mechanism for the token.

The above contracts are to be released on two blockchains simultaneously: Ethereum and Polygon.

# Contracts

#### ExenoToken

This is the base contract for Exeno Coin (EXN). It implements the [ERC-1363](https://eips.ethereum.org/EIPS/eip-1363) standard, which is an extension of the well-known ERC-20 standard. 

The ERC-1363 standard turns the contract into a payable token, as described in [Vittorio Minacori's blog post](https://medium.com/coinmonks/ethereum-payable-token-and-how-it-works-3bf3349a6a77). This payable feature is utilized in the *ExenoTokenStaking* contract and is likely to be utilized further in the future, as the ecosystem around Exeno Coin evolves.

Another important feature is bridgeablity. Exeno Coin is meant to be deployed on multiple EVM-based blockchains simultaneously:

* Ethereum is intended to be the base blockchain where the initial (and final) supply is going to be minted. 
* Polygon is intended to be the primary blockchain for smart-contract operations, especially in cases where transaction fees matter.

[Polygon POS Bridge](https://wallet.polygon.technology) is applied for bridging between Ethereum and Polygon (in both directions). Other L2 blockchains are going to be bridged from Polygon by other means - most likely via the [NXTP](https://github.com/connext/nxtp) protocol, while no further bridging is planned for the Ethereum blockchain. The sum of total supply on all blockchains is intended to remain constant (i.e. 500,000,000 tokens).

To achieve the above goal, the *ExenoToken* contract is going to be deployed as two separate instances:

* *ExenoToken_Layer1* is intended to be used for deployment on the Ethereum blockchain. When deployed, the contract is supposed to mint the initial (and final) supply of 500,000,000 tokens. Also, this contract is ownable and the owner has the right to burn an arbitrary amount of their own EXN tokens.
* *ExenoToken_Layer2* is intended to be used for deployment on the Polygon blockchain. This contract does not mint any tokens when it is deployed, but instead fulfills the requirements of [standard child contract](https://github.com/maticnetwork/pos-portal/blob/master/contracts/child/ChildToken/ChildERC20.sol) (*ChildERC20*), as defined by [Polygon POS Bridge](https://docs.polygon.technology/docs/develop/ethereum-polygon/getting-started).

Additionally, purely for testing purposes, there are two mock-up contracts (which are not meant to be audited, as they will not be deployed in the production environment):

* *Mockup_ExenoToken* morphs into one of the real-life contracts (i.e. *ExenoToken_Layer1* or *ExenoToken_Layer2*). Also, please note that when *ExenoToken* morphs into *ExenoToken_Layer2* the mock-up simulates the state of the token after it is already bridged to Polygon.
* *Mockup_PriceFeed* simulates a [Chainlink](https://docs.chain.link/docs/architecture-overview/) price feed for retrieving the market value of the blockchain's native currency: ETH or MATIC.

#### ExenoTokenIco

This contract facilitates the process of the intended public sale of Exeno Coin.

*ExenoTokenIco* implements a relatively simple workflow: an investor sends to the contract some amount of the blockchain's native currency (i.e. ETH or MATIC) and in response the contract sends to the investor the appropriate amount of EXN tokens. This exchange is immediate and no vesting is applied.

The public sale will take place on two blockchains simultaneously, with investors being free to choose the blockchain and means of payment: ETH or MATIC.

To realize this goal, *ExenoTokenIco* will be deployed in the same form on two blockchains (i.e. Ethereum and Polygon). The only difference will be applied in the constructor parameters in order to provide the appropriate address of the Chainlink-based price feed, as this address differs depending on the blockchain.

The *ExenoTokenIco* contract is coupled with an instance of the *ExenoToken* contract (i.e. an instance of *ExenoToken_Layer1* or *ExenoToken_Layer2* - depending on the target blockchain).

Main features of *ExenoTokenIco*:

* The contract has a pre-defined start date. No tokens can be sold before the start date.
* EXN tokens can be purchased by either invoking the *buyTokens* method or by simply sending some amount of blockchain's native currency to the contract. If the *buyTokens* method is used the beneficiary can be different than the payer.
* The contract itself does not hold any EXN tokens. Instead, the owner of the contract needs to approve an allowance and the purchased tokens are distributed to investors from this allowance. Failing to provide this allowance will prevent investors from making further purchases of the token.
* The price is expressed in USD, while the payment is done in the blockchain's native currency. To keep the value of Exeno Coin stable in terms of USD, the contract utilizes a Chainlink price feed each time a purchase takes place. The current market value of the blockchain's native currency is used to calculate its purchasing power, as the market fluctuates.
* The process of token sale is divided into three stages: *PreICO*, *ICO*, *PostICO*. The *PreICO* stage is intended to give the investor a better price than the *ICO* stage. Whereas the *PostICO* stage permanently disables further sale of the token.
* The investor is not allowed to buy less than the pre-defined minimum or more than the pre-defined maximum of EXN tokens, no matter how many times the act of purchasing is performed. As this condition is monitored on an investor's account level, the maximum cap cannot be enforced if an investor uses multiple accounts to purchase tokens.
* The contract has a built-in logic to switch from the current stage to the next, based on the amount of tokens sold so far. The thresholds for those transitions are pre-defined in the contract:
  * When 50 million tokens are sold, the *ICO* stage will automatically kick in.
  * When 75 million tokens are sold, the *PostICO* stage will automatically kick in.
* At any moment the owner of the contract is allowed to promote the contract to the next stage (but is not allowed to revert to the previous stage).
* Whenever the stage is changed (whether automatically or manually) the exchange rate is automatically updated to the corresponding pre-defined level for this new stage. However, at any moment the owner of the contract is allowed to amend the current exchange rate.
* At any moment the owner of the contract is allowed to pause (and then unpause) the sale of the token.
* At any moment the owner of the contract is allowed to forward the cash accumulated in the contract (i.e. ETH or MATIC) to the pre-defined wallet address.

#### ExenoTokenStaking

This contract enables Exeno Coin holders to stake all or part of their holdings, and in the future (when the stake is withdrawn) to receive their tokens back, plus an interest payout as a reward. The interest payout is payed in EXN tokens, not native currency of a given blockchain.

*ExenoTokenStaking* implements the following workflow: 

* **Staking**: an investor sends to the contract some amount of EXN tokens and the contract locks those tokens and opens an individual staking deposit for the investor, noting the exact time and volume of the staking transaction.
* **Unstaking**: an investor triggers the appropriate method on the contract, indicating the amount of EXN tokens he or she wants to withdraw from the stake, and the contract unlocks this amount of tokens and sends them back to the investor, together with the appropriate interest payout.

There are three main parameters involved in staking:

* *Annualized Interest Rate* determines the amount of interest payout allocated to the investor proportionally to the duration of the staking deposit. It's expressed as a percentage number, e.g. 1285 means 0.1285 or 12.85% per annum.
* *Rebase Period In Hours* determines how often the staking reward gets added to the claimable interest payout. The aim is to incentivize token holders to keep their stake untouched at least as long as the rebase period, or its multiple. For example, a rebase period equal to 24 hours implies that exactly every 24 hours the claimable interest payout is increased by the amount of interest accumulated in the last 24 hours, and nothing happens outside of this 24-hour interval. As a result:
  * If an investor withdraws their stake after 23 hours and 59 minutes since the stake was initiated, he or she will not get any interest payout, only the staked tokens will be returned. 
  * After 24 hours, if the investor decides to unstake, he or she will receive the staked tokens plus interest corresponding to 24 hours of staking.
  * After 25 hours, the investor will still receive interest corresponding to 24 hours of staking (i.e. one hour less than the actual age of the staking deposit).
  * After 48 hours, the investor will receive interest corresponding to 48 hours of staking, and so on.
* *Stake Start Mode* determines the way starting time for new stakes is calculated. It can be immediate (i.e. a new stake starts when the transaction is sent to the blockchain) or delayed to the nearest full hour or specific time of the day (e.g. midnight UTC, 1:00 AM UTC, noon UTC, 1:00 PM UTC).

Similarly to the *ExenoTokenIco* contract, *ExenoTokenStaking* is going to be deployed on two blockchains simultaneously. And the intention is to have multiple versions of *ExenoTokenStaking* on each blockchain. Those various instances of *ExenoTokenStaking* will differ in terms of the *annualizedInterestRate* and *rebasePeriodInHours* parameters applied in their constructors, so that token holders are offered a choice matching their investment preferences (trade-off between a higher interest rate and a shorter deposit period).

And similarly to the *ExenoTokenIco* contract, *ExenoTokenStaking* is coupled with an instance of the *ExenoToken* contract (i.e. an instance of *ExenoToken_Layer1* or *ExenoToken_Layer2* - depending on the target blockchain).

Main features of *ExenoTokenStaking*:

* To initiate staking, a token holder is supposed to interact not with the *ExenoTokenStaking* contract, but instead with the *ExenoToken* contract by utilizing its payable feature. In other words, the token holder invokes the *transferAndCall* method with the amount he or she wants to stake and enters the address of the *ExenoTokenStaking* contract as the recipient.
* An investor can make multiple staking transactions and they can be done at different times. Each staking deposit is stored in the contract as a separate entry, with its own timestamp and amount. Each staking deposit is assigned its own index, starting with 0.
* To unstake the investor has two options:
  * Supply the index of the staking deposit which he or she wants to withdraw from. In this case the amount to be unstaked cannot exceed the amount staked under this index. Amount equal to zero indicates unstaking the maximum available amount.
  * Omit the index and just supply the amount to be unstaked. In this case the amount to be unstaked cannot exceed the total amount staked under all available indexes. Again, amount equal to zero indicates unstaking the maximum available amount.
* The contract holds all EXN tokens that are being staked. However, the contract does not hold funds for interest payouts. Instead, the owner of the contract needs to approve the appropriate allowance and interest payouts are distributed from this allowance.
* In order to prevent a very unlikely event of investors being unable to unstake their tokens due to lack of appropriate allowance for interest payouts, the contract offers a method to unstake without claiming an interest payout. However, this method is not meant to used by investors under normal circumstances, but only when there is no other way to reclaim their tokens.
* At any moment the owner of the contract is allowed to pause (and then unpause) the ability to create new staking deposits. However, this pausing does not affect unstaking or processing of interest payouts.
* The contract has a pre-defined end date. No tokens can be staked after the end date. Also, the interest payout stops accruing after the end date. However, any rebase period started before the end date is allowed to complete, even if it extends beyond the end date.
* At any moment the owner of the contract is allowed to change *Annualized Interest Rate*. If any change is made, it will apply to all stakes that have not been closed yet, while already unstaked deposits remain unaffected.
* At any moment the owner of the contract is allowed to change *Stake Start Mode*. If any change is made, it will apply only to newly created stakes, while stakes that are already started remain unchanged.
* At any moment the owner of the contract is allowed to extend the pre-defined end date.

# Installing

The project was developed using [Truffle](https://trufflesuite.com/) and was unit-tested using [Ganache](https://trufflesuite.com/ganache/).

To install just run:

```bash
npm install
```

This project targets version `0.8.4` of the Solidity compiler.

# Dependencies

To avoid potentially destructive mistakes, our aim was to leverage the existing Solidity libraries whenever possible.

The codebase of the following open-source projects have been utilized:

* [OpenZeppelin](https://github.com/OpenZeppelin/openzeppelin-contracts)
* [ERC-1363 Payable Token](https://github.com/vittominacori/erc1363-payable-token)
* [Matic Network POS Portal](https://github.com/maticnetwork/pos-portal)
* [Chainlink](https://github.com/smartcontractkit/chainlink)

Regarding the *Matic Network POS Portal* dependency: unfortunately Matic's latest release is compatible with version `0.6.6` of the Solidity compiler, while this project targets version `0.8.4`. Thus, the Matic library could not be imported via `npm`. Instead, the following files needed to be added manually to the codebase and slightly amended in order to upgrade them to version `0.8.4`:

```
/maticnetwork/pos-portal/contracts/child/ChildToken/IChildToken.sol
/maticnetwork/pos-portal/contracts/common/AccessControlMixin.sol
/maticnetwork/pos-portal/contracts/common/ContextMixin.sol
/maticnetwork/pos-portal/contracts/common/EIP712Base.sol
/maticnetwork/pos-portal/contracts/common/Initializable.sol
/maticnetwork/pos-portal/contracts/common/NativeMetaTransaction.sol
```

Here are the amendments done:

* **/maticnetwork/pos-portal/contracts/common/ContextMixin.sol**
  * In line 8: `returns (address payable sender)` is replaced by `returns (address sender)`

* **/maticnetwork/pos-portal/contracts/common/EIP712Base.sol**
  * In line 51: `function getChainId() public pure returns (uint256) {` is replaced by `function getChainId() public view returns (uint256) {`
* **/maticnetwork/pos-portal/contracts/common/NativeMetaTransaction.sol**
  * In line 4: `import {SafeMath} from "@openzeppelin/contracts/math/SafeMath.sol";` is replaced by `import {SafeMath} from "@openzeppelin/contracts/utils/math/SafeMath.sol";`
  * In line 16: `address payable relayerAddress,` is replaced by `address relayerAddress,`

# Testing

#### ExenoToken

To test general features of the *ExenoToken_Layer1* and *ExenoToken_Layer2* contracts, run:

```
truffle test .\test\ExenoToken.js
```

**Note**: To switch between *ExenoToken_Layer1* and *ExenoToken_Layer2* contracts you need to uncomment appropriate lines in the *Mockup_ExenoToken* file.

To test the ownable features of the *ExenoToken_Layer1* contract, run:

```
truffle test .\test\ExenoTokenOwnable.js
```

#### ExenoTokenIco

To test features of the *ExenoTokenIco* contract, run:

```
truffle test .\test\ico\ExenoTokenIco.js
```

#### ExenoTokenStaking

To test features of the *ExenoTokenStaking* contract, run:

```
truffle test .\test\staking\ExenoTokenStaking.js 
truffle test .\test\staking\ExenoTokenStakingEndDate.js
truffle test .\test\staking\ExenoTokenStakingFifo.js
```

To test the correctness of payout calculations in different scenarios, run:

```
truffle test .\test\staking\ExenoTokenUnstakingA.js
truffle test .\test\staking\ExenoTokenUnstakingB.js
truffle test .\test\staking\ExenoTokenUnstakingC.js
truffle test .\test\staking\ExenoTokenUnstakingD.js
truffle test .\test\staking\ExenoTokenUnstakingE.js
```
