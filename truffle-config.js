require('babel-register');
require('babel-polyfill');
require('dotenv').config();

const HDWalletProvider = require('@truffle/hdwallet-provider');

module.exports = {
  networks: {
    development: {
      host: 'localhost',
      port: 7545,
      gas: 6721975,
      gasPrice: 25000000000,
      network_id: '*',
    },
    ganache: {
      host: 'localhost',
      port: 7545,
      gas: 6721975,
      gasPrice: 25000000000,
      network_id: '*',
    },
    polygon_testnet: {
      provider: () => new HDWalletProvider(
        process.env.MNEMONIC,
        `https://rpc-mumbai.matic.today`),
      network_id: 80001
    },
    bsc_testnet: {
      provider: () => new HDWalletProvider(
        process.env.MNEMONIC,
        `https://data-seed-prebsc-1-s1.binance.org:8545`),
      network_id: 97,
      confirmations: 10,
      timeoutBlocks: 200,
      skipDryRun: true
    },
    ropsten: {
      provider: () => new HDWalletProvider(
          process.env.MNEMONIC,
          `https://ropsten.infura.io/${process.env.INFURA_API_KEY}`),
      gas: 5000000,
      gasPrice: 25000000000,
      network_id: 3
    }
  },
  compilers: {
    solc: {
      version: "^0.8.4",
    }
  },
  mocha: {
    reporter: 'eth-gas-reporter'
  },
  plugins: ["solidity-coverage"],
  solc: {
    optimizer: {
      enabled: true,
      runs: 200
    }
  }
};
