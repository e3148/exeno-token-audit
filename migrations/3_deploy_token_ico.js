const { toWei } = require("web3-utils");

const ExenoToken = artifacts.require("ExenoToken");
const ExenoTokenIco = artifacts.require("ExenoTokenIco");
const PriceFeed = artifacts.require("PriceFeed");

module.exports = async function(deployer, network, accounts) {

  const price = 166260638;
  const decimals = 8;

  await deployer.deploy(
    PriceFeed,
    price,
    decimals
  );
  
  const preIcoRate = 3500;
  const icoRate = 5000;
  const minCap = toWei("1");
  const maxCap = toWei("100000");
  const wallet = accounts[1];
  const startDate = Math.round(Date.parse("2021-01-01T00:00:00+0000") / 1000);

  await deployer.deploy(
    ExenoTokenIco,
    ExenoToken.address,
    PriceFeed.address,
    preIcoRate,
    icoRate,
    minCap,
    maxCap,
    wallet,
    startDate
  );

  return true;
}
