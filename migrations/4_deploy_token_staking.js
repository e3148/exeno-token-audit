const ExenoToken = artifacts.require("ExenoToken");
const ExenoTokenStaking = artifacts.require("ExenoTokenStaking");

module.exports = async function(deployer, network, accounts) {
  
  let annualizedInterestRate;
  let rebasePeriodInHours;
  let stakeStartMode;
  let endDate;

  annualizedInterestRate = 150;
  rebasePeriodInHours = 24;
  stakeStartMode = 0;
  endDate = Math.round(Date.parse("2222-12-31T23:59:59+0000") / 1000);
  await deployer.deploy(ExenoTokenStaking, ExenoToken.address, annualizedInterestRate, rebasePeriodInHours, stakeStartMode, endDate);

  annualizedInterestRate = 609;
  rebasePeriodInHours = 720;
  stakeStartMode = 0;
  endDate = Math.round(Date.parse("2222-02-22T22:22:22+0000") / 1000);
  await deployer.deploy(ExenoTokenStaking, ExenoToken.address, annualizedInterestRate, rebasePeriodInHours, stakeStartMode, endDate);

  annualizedInterestRate = 896;
  rebasePeriodInHours = 1440;
  stakeStartMode = 0;
  endDate = Math.round(Date.parse("2222-02-22T22:22:22+0000") / 1000);
  await deployer.deploy(ExenoTokenStaking, ExenoToken.address, annualizedInterestRate, rebasePeriodInHours, stakeStartMode, endDate);

  annualizedInterestRate = 1050;
  rebasePeriodInHours = 2160;
  stakeStartMode = 0;
  endDate = Math.round(Date.parse("2222-02-22T22:22:22+0000") / 1000);
  await deployer.deploy(ExenoTokenStaking, ExenoToken.address, annualizedInterestRate, rebasePeriodInHours, stakeStartMode, endDate);

  return true;
}
