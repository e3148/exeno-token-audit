const { constants, expectEvent, expectRevert } = require("@openzeppelin/test-helpers");
const { toWei, toBN } = require("web3-utils");

require('chai').use(require('chai-as-promised')).should();

const ExenoToken = artifacts.require("ExenoToken_Layer1");

contract("ExenoTokenOwnable", async (accounts) => {
    let token;

    beforeEach(async () => {
        token = await ExenoToken.new();
    });

    describe("transferring ownership", function () {
        describe("to a valid account", function () {
            it("should succeed & emit event", async () => {
                (await token.owner()).should.equal(accounts[0]);

                const receipt = await token.transferOwnership(accounts[1]);
                await expectEvent.inTransaction(receipt.tx, token, "OwnershipTransferred", {
                    previousOwner: accounts[0],
                    newOwner: accounts[1]
                });

                (await token.owner()).should.equal(accounts[1]);
            });
        });

        describe("to an invalid account", function () {
            it("should reject", async () => {
                (await token.owner()).should.equal(accounts[0]);

                await expectRevert(
                    token.transferOwnership(constants.ZERO_ADDRESS),
                    "Ownable: new owner is the zero address"
                );
            });
        });

        describe("done by a non-owner", function () {
            it("should reject", async () => {
                (await token.owner()).should.equal(accounts[0]);

                await expectRevert(
                    token.transferOwnership(accounts[1], { from: accounts[1] }),
                    "Ownable: caller is not the owner"
                );
            });
        });
    });

    describe("renouncing ownership", function () {
        describe("done by current owner", function () {
            it("should succeed & emit event", async () => {
                (await token.owner()).should.equal(accounts[0]);

                const receipt = await token.renounceOwnership({ from: accounts[0] });
                await expectEvent.inTransaction(receipt.tx, token, "OwnershipTransferred", {
                    previousOwner: accounts[0],
                    newOwner: constants.ZERO_ADDRESS
                });

                (await token.owner()).should.equal(constants.ZERO_ADDRESS);
            });
        });

        describe("done by a non-owner", function () {
            it("should reject", async () => {
                (await token.owner()).should.equal(accounts[0]);

                await expectRevert(
                    token.renounceOwnership({ from: accounts[1] }),
                    "Ownable: caller is not the owner"
                );
            });
        });
    });

    describe("burning", function () {
        it("burning by owner", async () => {
            const initialBalance = await token.balanceOf(accounts[0]);
            const initialSupply = await token.totalSupply();
            const burnAmount = toWei("5.8888");

            await token.methods['burn(uint256)'](burnAmount);

            // Ensure balance was properly reduced
            const newBalance = await token.balanceOf(accounts[0]);
            newBalance.should.be.bignumber.equal(initialBalance.sub(toBN(burnAmount)));

            // Ensure total supply was properly reduced
            (await token.totalSupply()).should.be.bignumber.equal(initialSupply.sub(toBN(burnAmount)));

            // Prevent burn more than balance
            await expectRevert(
                token.methods['burn(uint256)'](toWei(newBalance.add(toBN(toWei("0.01"))))),
                "ERC20: burn amount exceeds balance"
            );
        });

        it("burning by non-owner", async () => {
            await token.transfer(accounts[1], toWei("10"));
            await expectRevert(
                token.methods['burn(uint256)'](toWei("10"), { from: accounts[1] }),
                "Ownable: caller is not the owner"
            );
        });
    });
});