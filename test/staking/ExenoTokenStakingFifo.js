const { expectRevert, expectEvent } = require("@openzeppelin/test-helpers");
const { toWei, toBN } = require("web3-utils");
const { advanceTimeAndBlock, getBlock, period } = require("../../utils/time");
const { formatStatus } = require("../../utils/format");

require('chai').use(require('chai-as-promised')).should();

const ExenoToken = artifacts.require("ExenoToken");
const ExenoTokenStaking = artifacts.require("ExenoTokenStaking");

contract("ExenoTokenStakingFifo", async (accounts) => {
    let token;
    let staking;

    beforeEach(async () => {
        const block = await getBlock();

        token = await ExenoToken.new();
        staking = await ExenoTokenStaking.new(
            token.address, 876, 24, 0, block.timestamp + period.years(1));

        await token.approve(staking.address, toWei("1500"));
        await token.transfer(accounts[1], toWei("1500"));
        await token.transfer(accounts[2], toWei("1500"));
    });

    describe("unstaking", function () {
        it("succeeds & emits event", async () => {
            const amount1 = toWei("31.5253");
            const amount2 = toWei("0.3278");
            const amount3 = toWei("12.2223");
            const totalAmount = toBN(amount1).add(toBN(amount2)).add(toBN(amount3));
            const unstakeAmount = toWei("40.1111");

            await token.transferAndCall(staking.address, amount1, { from: accounts[1] });
            await advanceTimeAndBlock(period.hours(3));
            await token.transferAndCall(staking.address, amount2, { from: accounts[1] });
            await advanceTimeAndBlock(period.hours(49));
            await token.transferAndCall(staking.address, amount3, { from: accounts[1] });

            console.log(formatStatus(await staking.getStakingStatus(accounts[1])));

            await advanceTimeAndBlock(period.hours(28));

            const receipt = await staking.unstakeFifo(unstakeAmount, { from: accounts[1] });
            await expectEvent.inTransaction(receipt.tx, staking, "Unstaked", {
                user: accounts[1],
                amount: unstakeAmount
            });

            (await staking.totalTokensStaked()).should.be.bignumber.equal(toBN(totalAmount).sub(toBN(unstakeAmount)));
            (await staking.getNumberOfStakeholders()).should.be.bignumber.equal(toBN("1"));
        });

        it("unstakes everything when amount is zero", async () => {
            const amount1 = toWei("31.5253");
            const amount2 = toWei("0.3278");
            const amount3 = toWei("12.2223");
            const totalAmount = toBN(amount1).add(toBN(amount2)).add(toBN(amount3));

            await token.transferAndCall(staking.address, amount1, { from: accounts[1] });
            await advanceTimeAndBlock(period.hours(3));
            await token.transferAndCall(staking.address, amount2, { from: accounts[1] });
            await advanceTimeAndBlock(period.hours(49));
            await token.transferAndCall(staking.address, amount3, { from: accounts[1] });

            console.log(formatStatus(await staking.getStakingStatus(accounts[1])));

            await advanceTimeAndBlock(period.hours(28));

            const receipt = await staking.unstakeFifo(0, { from: accounts[1] });
            await expectEvent.inTransaction(receipt.tx, staking, "Unstaked", {
                user: accounts[1],
                amount: totalAmount
            });
        });

        it("rejects the transaction if an account has never made any staking", async () => {
            const status = await staking.getStakingStatus(accounts[9]);
            console.log(formatStatus(status));

            status.totalAmount.should.be.bignumber.equal(toBN("0"));

            await expectRevert(
                staking.unstakeFifo(toWei("100"), { from: accounts[9] }),
                "ExenoTokenStaking: this account has never made any staking"
            );

            await expectRevert(
                staking.unstakeFifo(0, { from: accounts[9] }),
                "ExenoTokenStaking: this account has never made any staking"
            );
        });
    });

    describe("executing payouts", async () => {
        it("tracks the balances", async () => {
            const stakingAmount1 = toWei("12.89");
            const stakingAmount2 = toWei("99.4734");
            const stakingAmount3 = toWei("5.128");

            const unstakingAmount1 = toWei("26.85");
            const unstakingAmount2 = toWei("75.00");
            const unstakingAmount3 = toWei("15.6414");

            const stakingDuration1 = period.hours(25);
            const stakingDuration2 = period.hours(49);
            const stakingDuration3 = period.hours(121);

            const unstakingDuration1 = period.hours(85);
            const unstakingDuration2 = period.hours(33);

            const expectedPayout1 = toWei("0.0482016");
            const expectedPayout2 = toWei("0.18");
            const expectedPayout3 = toWei("0.041355072");

            const initialStakerBalance = await token.balanceOf(accounts[1]);

            await token.transferAndCall(staking.address, stakingAmount1, { from: accounts[1] });
            console.log(formatStatus(await staking.getStakingStatus(accounts[1])));


            await advanceTimeAndBlock(stakingDuration1);


            await token.transferAndCall(staking.address, stakingAmount2, { from: accounts[1] });
            console.log(formatStatus(await staking.getStakingStatus(accounts[1])));


            await advanceTimeAndBlock(stakingDuration2);


            await token.transferAndCall(staking.address, stakingAmount3, { from: accounts[1] });
            console.log(formatStatus(await staking.getStakingStatus(accounts[1])));


            await advanceTimeAndBlock(stakingDuration3);


            console.log(formatStatus(await staking.getStakingStatus(accounts[1])));

            await staking.unstakeFifo(unstakingAmount1, { from: accounts[1] });
            console.log(formatStatus(await staking.getStakingStatus(accounts[1])));

            const newStakerBalance1 = await token.balanceOf(accounts[1]);
            const expectedStakerBalance1 = toBN(initialStakerBalance)
                .sub(toBN(stakingAmount1))
                .sub(toBN(stakingAmount2))
                .sub(toBN(stakingAmount3))
                .add(toBN(unstakingAmount1))
                .add(toBN(expectedPayout1));
            newStakerBalance1.should.be.bignumber.equal(expectedStakerBalance1);


            await advanceTimeAndBlock(unstakingDuration1);

            console.log(formatStatus(await staking.getStakingStatus(accounts[1])));

            await staking.unstakeFifo(unstakingAmount2, { from: accounts[1] });
            console.log(formatStatus(await staking.getStakingStatus(accounts[1])));

            const newStakerBalance2 = await token.balanceOf(accounts[1]);
            const expectedStakerBalance2 = toBN(newStakerBalance1)
                .add(toBN(unstakingAmount2))
                .add(toBN(expectedPayout2));
            newStakerBalance2.should.be.bignumber.equal(expectedStakerBalance2);


            await advanceTimeAndBlock(unstakingDuration2);


            console.log(formatStatus(await staking.getStakingStatus(accounts[1])));

            await staking.unstakeFifo(unstakingAmount3, { from: accounts[1] });
            console.log(formatStatus(await staking.getStakingStatus(accounts[1])));

            const newStakerBalance3 = await token.balanceOf(accounts[1]);
            const expectedStakerBalance3 = toBN(newStakerBalance2)
                .add(toBN(unstakingAmount3))
                .add(toBN(expectedPayout3));
            newStakerBalance3.should.be.bignumber.equal(expectedStakerBalance3);
        });
    });

    describe("unstaking more than possible", async () => {
        it("rejects the transaction but then allows it when the amount is feasible", async () => {
            await token.transferAndCall(staking.address, toWei("30.00"), { from: accounts[2] });
            await token.transferAndCall(staking.address, toWei("20.00"), { from: accounts[2] });
            await token.transferAndCall(staking.address, toWei("50.00"), { from: accounts[2] });

            await advanceTimeAndBlock(period.hours(48));

            console.log(formatStatus(await staking.getStakingStatus(accounts[2])));

            await expectRevert(
                staking.unstakeFifo(toWei("101"), { from: accounts[2] }),
                "ExenoTokenStaking: not enough stake on all indexes for the requested amount"
            );

            await staking.unstakeFifo(toWei("100"), { from: accounts[2] });

            console.log(formatStatus(await staking.getStakingStatus(accounts[2])));
        });
    });

    describe("not enough funds for rewards due to low balance", async () => {
        it("rejects the transaction but then allows it when owner's balance is refilled", async () => {
            (await staking.owner()).should.equal(accounts[0]);

            // The owner transfers everything but 0.19007 to some other account
            const ownerBalance = await token.balanceOf(accounts[0]);
            await token.transfer(accounts[9], ownerBalance.sub(toBN(toWei("0.19007"))), { from: accounts[0] });
            (await token.balanceOf(accounts[0])).should.be.bignumber.equal(toBN(toWei("0.19007")));

            await token.transferAndCall(staking.address, toWei("30.00"), { from: accounts[1] });
            await token.transferAndCall(staking.address, toWei("20.00"), { from: accounts[1] });
            await token.transferAndCall(staking.address, toWei("50.00"), { from: accounts[1] });

            await advanceTimeAndBlock(period.hours(200));

            console.log(formatStatus(await staking.getStakingStatus(accounts[1])));

            const amount = toWei("99.00");

            await expectRevert(
                staking.unstakeFifo(amount, { from: accounts[1] }),
                "ExenoTokenStaking: not enough balance for payout"
            );

            await token.transfer(accounts[0], toWei("0.00001"), { from: accounts[2] });

            await staking.unstakeFifo(amount, { from: accounts[1] });

            console.log(formatStatus(await staking.getStakingStatus(accounts[1])));
        });

        it("rejects the transaction but then allows it when the stakeholder gives up on payout", async () => {
            (await staking.owner()).should.equal(accounts[0]);

            // The owner transfers everything but 0.19007 to some other account
            const ownerBalance = await token.balanceOf(accounts[0]);
            await token.transfer(accounts[9], ownerBalance.sub(toBN(toWei("0.19007"))), { from: accounts[0] });
            (await token.balanceOf(accounts[0])).should.be.bignumber.equal(toBN(toWei("0.19007")));

            await token.transferAndCall(staking.address, toWei("30.00"), { from: accounts[1] });
            await token.transferAndCall(staking.address, toWei("20.00"), { from: accounts[1] });
            await token.transferAndCall(staking.address, toWei("50.00"), { from: accounts[1] });

            await advanceTimeAndBlock(period.hours(200));

            await staking.getStakingStatus(accounts[1]);
            console.log(formatStatus(await staking.getStakingStatus(accounts[1])));

            const amount = toWei("99.00");

            await expectRevert(
                staking.unstakeFifo(amount, { from: accounts[1] }),
                "ExenoTokenStaking: not enough balance for payout"
            );

            // Yet unstaking should be possible without payout
            const receipt = await staking.unstakeFifoWithoutPayout(amount, { from: accounts[1] });
            await expectEvent.inTransaction(receipt.tx, staking, "Unstaked", {
                user: accounts[1],
                amount: amount,
                payout: toWei("0")
            });

            console.log(formatStatus(await staking.getStakingStatus(accounts[1])));
        });
    });

    describe("not enough funds for rewards due to low allowance", async () => {
        it("rejects the transaction but then allows it when allowance is reinstated", async () => {
            (await staking.owner()).should.equal(accounts[0]);

            // The owner reduces allowance to 0.19007
            await token.approve(staking.address, toWei("0.19007"), { from: accounts[0] });
            (await token.allowance(accounts[0], staking.address)).should.be.bignumber.equal(toBN(toWei("0.19007")));

            await token.transferAndCall(staking.address, toWei("30.00"), { from: accounts[1] });
            await token.transferAndCall(staking.address, toWei("20.00"), { from: accounts[1] });
            await token.transferAndCall(staking.address, toWei("50.00"), { from: accounts[1] });

            await advanceTimeAndBlock(period.hours(200));

            console.log(formatStatus(await staking.getStakingStatus(accounts[1])));

            const amount = toWei("99.00");

            await expectRevert(
                staking.unstakeFifo(amount, { from: accounts[1] }),
                "ExenoTokenStaking: not enough allowance for payout"
            );

            // The owner increases allowance to 0.19008
            await token.approve(staking.address, toWei("0.19008"), { from: accounts[0] });

            await staking.unstakeFifo(amount, { from: accounts[1] });

            console.log(formatStatus(await staking.getStakingStatus(accounts[1])));
        });

        it("rejects the transaction but then allows it when the stakeholder gives up on payout", async () => {
            (await staking.owner()).should.equal(accounts[0]);

            // The owner reduces allowance to 0.19007
            await token.approve(staking.address, toWei("0.19007"), { from: accounts[0] });
            (await token.allowance(accounts[0], staking.address)).should.be.bignumber.equal(toBN(toWei("0.19007")));

            await token.transferAndCall(staking.address, toWei("30.00"), { from: accounts[1] });
            await token.transferAndCall(staking.address, toWei("20.00"), { from: accounts[1] });
            await token.transferAndCall(staking.address, toWei("50.00"), { from: accounts[1] });

            await advanceTimeAndBlock(period.hours(200));

            console.log(formatStatus(await staking.getStakingStatus(accounts[1])));

            const amount = toWei("99.00");

            await expectRevert(
                staking.unstakeFifo(amount, { from: accounts[1] }),
                "ExenoTokenStaking: not enough allowance for payout"
            );

            // Yet unstaking should be possible without payout
            const receipt = await staking.unstakeFifoWithoutPayout(amount, { from: accounts[1] });
            await expectEvent.inTransaction(receipt.tx, staking, "Unstaked", {
                user: accounts[1],
                amount: amount,
                payout: toWei("0")
            });

            console.log(formatStatus(await staking.getStakingStatus(accounts[1])));
        });
    });
});