const { expectEvent } = require("@openzeppelin/test-helpers");
const { toWei, toBN } = require("web3-utils");
const { advanceTimeAndBlock, getBlock, getDate, period } = require("../../utils/time");
const { formatStatus, formatDate } = require("../../utils/format");

require('chai').use(require('chai-as-promised')).should();

const ExenoToken = artifacts.require("ExenoToken");
const ExenoTokenStaking = artifacts.require("ExenoTokenStaking");

contract("ExenoTokenUnstakingC", async (accounts) => {
    let token;
    let staking;
    let date;
    let status;

    beforeEach(async () => {
        const block = await getBlock();

        token = await ExenoToken.new();
        staking = await ExenoTokenStaking.new(
            token.address, 1752, 168, 0, block.timestamp + period.years(1));

        await token.approve(staking.address, toWei("100"));
        await token.transfer(accounts[1], toWei("1500"));
        await token.transfer(accounts[2], toWei("1500"));
    });

    it("calculating rewards", async () => {
        // Initial staking of 100 tokens
        console.log(`\nInitial staking of 100 tokens (${formatDate(await getDate())})`);
        await token.transferAndCall(staking.address, toWei("100"), { from: accounts[1] });


        // Fast-forward 11h, i.e. 11h for stake 1
        date = await advanceTimeAndBlock(period.hours(11));

        status = await staking.getStakingStatus(accounts[1]);
        console.log(`\nFast-forward 11h (${formatDate(date)})`);
        console.log(formatStatus(status));

        // Reward after 11h for staking 100 tokens
        status.stakes[0].interest.should.be.bignumber.equal(toBN(toWei("0.022")));
        status.stakes[0].claimable.should.be.bignumber.equal(toBN(toWei("0")));


        // Fast-forward 13h, i.e. 24h for stake 1
        date = await advanceTimeAndBlock(period.hours(13));

        status = await staking.getStakingStatus(accounts[1]);
        console.log(`\nFast-forward 13h (${formatDate(date)})`);
        console.log(formatStatus(status));

        // Reward after 24h for staking 100 tokens
        status.stakes[0].interest.should.be.bignumber.equal(toBN(toWei("0.048")));
        status.stakes[0].claimable.should.be.bignumber.equal(toBN(toWei("0")));


        // Further staking of 1200.45 tokens
        console.log("\nFurther staking of 1200.45 tokens");
        await token.transferAndCall(staking.address, toWei("1200.45"), { from: accounts[1] });


        // Fast-forward 20h, i.e. 44h for stake 1 and 20h for stake 2
        date = await advanceTimeAndBlock(period.hours(20));

        status = await staking.getStakingStatus(accounts[1]);
        console.log(`\nFast-forward 20h (${formatDate(date)})`);
        console.log(formatStatus(status));

        // Reward after 44h for staking 100 tokens
        status.stakes[0].interest.should.be.bignumber.equal(toBN(toWei("0.088")));
        status.stakes[0].claimable.should.be.bignumber.equal(toBN(toWei("0")));

        // Reward after 20h for staking 1200.45 tokens
        status.stakes[1].interest.should.be.bignumber.equal(toBN(toWei("0.48018")));
        status.stakes[1].claimable.should.be.bignumber.equal(toBN(toWei("0")));


        // Fast-forward 5h, i.e. 49h for stake 1 and 25h for stake 2
        date = await advanceTimeAndBlock(period.hours(5));

        status = await staking.getStakingStatus(accounts[1]);
        console.log(`\nFast-forward 5h (${formatDate(date)})`);
        console.log(formatStatus(status));

        // Reward after 49h for staking 100 tokens
        status.stakes[0].interest.should.be.bignumber.equal(toBN(toWei("0.098")));
        status.stakes[0].claimable.should.be.bignumber.equal(toBN(toWei("0")));

        // Reward after 25h for staking 1200.45 tokens
        status.stakes[1].interest.should.be.bignumber.equal(toBN(toWei("0.600225")));
        status.stakes[1].claimable.should.be.bignumber.equal(toBN(toWei("0")));


        // Fast-forward 150h, i.e. 199h for stake 1 and 175h for stake 2
        date = await advanceTimeAndBlock(period.hours(150));

        status = await staking.getStakingStatus(accounts[1]);
        console.log(`\nFast-forward 150h (${formatDate(date)})`);
        console.log(formatStatus(status));

        // Reward after 199h for staking 100 tokens
        status.stakes[0].interest.should.be.bignumber.equal(toBN(toWei("0.398")));
        status.stakes[0].claimable.should.be.bignumber.equal(toBN(toWei("0.336")));

        // Reward after 175h for staking 1200.45 tokens
        status.stakes[1].interest.should.be.bignumber.equal(toBN(toWei("4.201575")));
        status.stakes[1].claimable.should.be.bignumber.equal(toBN(toWei("4.033512")));
    });

    it("executing payouts", async () => {
        const stakingAmount = toWei("25.517");
        const unstakingAmount1 = toWei("11.285");
        const unstakingAmount2 = toWei("14.232");

        (await staking.owner()).should.equal(accounts[0]);
        const initialOwnerBalance = await token.balanceOf(accounts[0]);
        const initialStakerBalance = await token.balanceOf(accounts[2]);

        await token.transferAndCall(staking.address, stakingAmount, { from: accounts[2] });

        status = await staking.getStakingStatus(accounts[2]);
        console.log(formatStatus(status, await getDate()));


        await advanceTimeAndBlock(period.hours(170));


        status = await staking.getStakingStatus(accounts[2]);
        console.log(formatStatus(status));

        const receipt1 = await staking.unstake(unstakingAmount1, 0, { from: accounts[2] });
        const payout1 = toWei("0.0379176");

        // Ensure payout is as expected
        await expectEvent.inTransaction(receipt1.tx, staking, "Unstaked", {
            user: accounts[2],
            amount: unstakingAmount1,
            payout: payout1
        });

        status = await staking.getStakingStatus(accounts[2]);
        console.log(formatStatus(status));

        // Ensure payout is paid from the contract owner
        const newOwnerBalance1 = await token.balanceOf(accounts[0]);
        initialOwnerBalance.sub(newOwnerBalance1).should.be.bignumber.equal(toBN(payout1));

        // Ensure payout is paid to the staker
        const newStakerBalance1 = await token.balanceOf(accounts[2]);
        newStakerBalance1.should.be.bignumber.equal(toBN(initialStakerBalance)
            .sub(toBN(stakingAmount)).add(toBN(unstakingAmount1)).add(toBN(payout1)));


        await advanceTimeAndBlock(period.hours(180));


        status = await staking.getStakingStatus(accounts[2]);
        console.log(formatStatus(status));

        const receipt2 = await staking.unstake(unstakingAmount2, 0, { from: accounts[2] });
        const payout2 = toWei("0.09563904");

        // Ensure payout is as expected
        await expectEvent.inTransaction(receipt2.tx, staking, "Unstaked", {
            user: accounts[2],
            amount: unstakingAmount2,
            payout: payout2
        });

        status = await staking.getStakingStatus(accounts[2]);
        console.log(formatStatus(status));

        // Ensure payout is paid from the contract owner
        const newOwnerBalance2 = await token.balanceOf(accounts[0]);
        newOwnerBalance1.sub(newOwnerBalance2).should.be.bignumber.equal(toBN(payout2));

        // Ensure payout is paid to the staker
        const newStakerBalance2 = await token.balanceOf(accounts[2]);
        newStakerBalance2.should.be.bignumber.equal(toBN(newStakerBalance1)
            .add(toBN(unstakingAmount2)).add(toBN(payout2)));
    });
});