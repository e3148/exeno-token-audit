const { constants, expectRevert, expectEvent } = require("@openzeppelin/test-helpers");
const { toWei, toBN } = require("web3-utils");
const { advanceTimeAndBlock, getBlock, period } = require("../../utils/time");
const { formatStatus } = require("../../utils/format");

require('chai').use(require('chai-as-promised')).should();

const ExenoToken = artifacts.require("ExenoToken");
const ExenoTokenStaking = artifacts.require("ExenoTokenStaking");

contract("ExenoTokenStaking", async (accounts) => {
    const annualizedInterestRate = 1500;
    const rebasePeriodInHours = 24;

    let block;
    let stakeStartMode;
    let endDate;
    let token;
    let staking;

    const slippage = async (account) => {
        const status = await staking.getStakingStatus(account);
        return status.stakes[0].timestamp - status.timestamp;
    }

    beforeEach(async () => {
        block = await getBlock();
        stakeStartMode = 3;
        endDate = block.timestamp + period.years(1);

        token = await ExenoToken.new();
        staking = await ExenoTokenStaking.new(
            token.address, annualizedInterestRate, rebasePeriodInHours, stakeStartMode, endDate);

        await token.approve(staking.address, toWei("1000"));
        await token.transfer(accounts[1], toWei("5000"));
        await token.transfer(accounts[2], toWei("5000"));
    });

    describe("contract creation", function () {
        describe("when interest rate is invalid", function () {
            it("rejects creation", async function () {
                await expectRevert(
                    ExenoTokenStaking.new(token.address, 0, rebasePeriodInHours, stakeStartMode, block.timestamp + period.hours(1)),
                    "ExenoTokenStaking: invalid annualized interest rate"
                );
            });
        });
        describe("when rebase period is invalid", function () {
            it("rejects creation", async function () {
                await expectRevert(
                    ExenoTokenStaking.new(token.address, annualizedInterestRate, 0, stakeStartMode, block.timestamp + period.hours(1)),
                    "ExenoTokenStaking: invalid rebase period"
                );
            });
        });
        describe("when end-date is already expired", function () {
            it("rejects creation", async function () {
                await expectRevert(
                    ExenoTokenStaking.new(token.address, annualizedInterestRate, rebasePeriodInHours, stakeStartMode, block.timestamp - period.hours(1)),
                    "ExenoTokenStaking: end-date cannot be in the past"
                );
            });
        });
    });

    describe('contract parameters', function () {
        it('tracks annualizedInterestRate', async function () {
            (await staking.annualizedInterestRate()).should.be.bignumber.equal(toBN(annualizedInterestRate));
        });
        it('tracks rebasePeriodInHours', async function () {
            (await staking.rebasePeriodInHours()).should.be.bignumber.equal(toBN(rebasePeriodInHours));
        });
        it('tracks stakeStartMode', async function () {
            (await staking.stakeStartMode()).should.be.bignumber.equal(toBN(stakeStartMode));
        });
        it('tracks endDate', async function () {
            (await staking.endDate()).should.be.bignumber.equal(toBN(endDate));
        });
    });

    describe('updating contract parameters', function () {

        it('allows owner to update annualized interest rate', async function () {
            const newAnnualizedInterestRate = 555;
            await staking.updateAnnualizedInterestRate(newAnnualizedInterestRate);
            (await staking.annualizedInterestRate()).should.be.bignumber.equal(toBN(newAnnualizedInterestRate));
        });

        it('allows owner to update stake-start-mode', async function () {
            const newStakeStartMode = 0;
            await staking.updateStakeStartMode(newStakeStartMode);
            (await staking.stakeStartMode()).should.be.bignumber.equal(toBN(newStakeStartMode));
        });

        it('prevents non-owner from doing updates', async function () {
            const nonOwner = accounts[9];
            
            await expectRevert(
                staking.updateAnnualizedInterestRate(666, { from: nonOwner }),
                "Ownable: caller is not the owner"
            );
            
            await expectRevert(
                staking.updateStakeStartMode(0, { from: nonOwner }),
                "Ownable: caller is not the owner"
            );
        });
    });

    describe('contract pausing', async function () {
        it("should be able to be paused and unpaused by the owner", async function () {
            const nonOwner = accounts[9];

            (await staking.owner()).should.equal(accounts[0]);

            await expectRevert(
                staking.pause({ from: nonOwner }),
                "Ownable: caller is not the owner"
            );
            (await staking.paused()).should.equal(false);

            await staking.pause({ from: accounts[0] });
            (await staking.paused()).should.equal(true);

            await expectRevert(
                staking.unpause({ from: nonOwner }),
                "Ownable: caller is not the owner"
            );
            (await staking.paused()).should.equal(true);

            await staking.unpause({ from: accounts[0] });
            (await staking.paused()).should.equal(false);
        });
    });

    describe("staking", function () {
        describe("single transaction", function () {
            it("succeeds & emits event", async () => {
                const stakeAmount = toWei("80.1234");

                const receipt = await token.transferAndCall(staking.address, stakeAmount, { from: accounts[1] });
                await expectEvent.inTransaction(receipt.tx, staking, "Staked", {
                    user: accounts[1],
                    amount: stakeAmount
                });

                (await staking.totalTokensStaked()).should.be.bignumber.equal(toBN(stakeAmount));
                (await staking.getNumberOfStakeholders()).should.be.bignumber.equal(toBN("1"));
            });

            it("tracks the balance", async () => {
                const stakeAmount = toWei("80.1234");

                const totalSupply = await token.totalSupply();

                const stakerBalance1 = await token.balanceOf(accounts[1]);
                stakerBalance1.should.be.bignumber.equal(toBN(toWei("5000")));

                const contractBalance1 = await token.balanceOf(staking.address);
                contractBalance1.should.be.bignumber.equal(toBN("0"));

                await token.transferAndCall(staking.address, stakeAmount, { from: accounts[1] });

                const status = await staking.getStakingStatus(accounts[1]);
                console.log(formatStatus(status));

                status.totalAmount.should.be.bignumber.equal(toBN(stakeAmount));
                status.stakes[0].amount.should.be.bignumber.equal(toBN(stakeAmount));
                status.stakes[0].user.should.equal(accounts[1]);

                const stakerBalance2 = await token.balanceOf(accounts[1]);
                const contractBalance2 = await token.balanceOf(staking.address);

                stakerBalance2.should.be.bignumber.equal(stakerBalance1.sub(toBN(stakeAmount)));
                contractBalance2.should.be.bignumber.equal(contractBalance1.add(toBN(stakeAmount)));

                // Ensure the token's total supply is uneffected
                (await token.totalSupply()).should.be.bignumber.equal(totalSupply);
            });

            it("blocked when paused but works when unpaused", async () => {
                await staking.pause({ from: accounts[0] });
                (await staking.paused()).should.equal(true);

                const stakeAmount = toWei("13.0090");
                await expectRevert(
                    token.transferAndCall(staking.address, stakeAmount, { from: accounts[1] }),
                    "Pausable: paused"
                );

                await staking.unpause({ from: accounts[0] });
                (await staking.paused()).should.equal(false);
                await token.transferAndCall(staking.address, stakeAmount, { from: accounts[1] });
            });
        });

        describe("multiple stakings from the same account", function () {
            it("all succeed & emit events", async () => {
                const stakeAmount1 = toWei("22.4365");
                const stakeAmount2 = toWei("16.5000");

                const receipt1 = await token.transferAndCall(staking.address, stakeAmount1, { from: accounts[1] });
                const receipt2 = await token.transferAndCall(staking.address, stakeAmount2, { from: accounts[1] });

                await expectEvent.inTransaction(receipt1.tx, staking, "Staked", {
                    user: accounts[1],
                    amount: stakeAmount1
                });

                await expectEvent.inTransaction(receipt2.tx, staking, "Staked", {
                    user: accounts[1],
                    amount: stakeAmount2
                });

                (await staking.totalTokensStaked()).should.be.bignumber.equal(toBN(stakeAmount1).add(toBN(stakeAmount2)));
                (await staking.getNumberOfStakeholders()).should.be.bignumber.equal(toBN("1"));
            });
        });

        describe("multiple stakings from different accounts", function () {
            it("all succeed & emit events", async () => {
                const stakeAmount1 = toWei("12.3847");
                const stakeAmount2 = toWei("5.5983");

                const receipt1 = await token.transferAndCall(staking.address, stakeAmount1, { from: accounts[1] });
                const receipt2 = await token.transferAndCall(staking.address, stakeAmount2, { from: accounts[2] });

                await expectEvent.inTransaction(receipt1.tx, staking, "Staked", {
                    user: accounts[1],
                    amount: stakeAmount1
                });

                await expectEvent.inTransaction(receipt2.tx, staking, "Staked", {
                    user: accounts[2],
                    amount: stakeAmount2
                });

                (await staking.totalTokensStaked()).should.be.bignumber.equal(toBN(stakeAmount1).add(toBN(stakeAmount2)));
                (await staking.getNumberOfStakeholders()).should.be.bignumber.equal(toBN("2"));
            });
        });

        describe("more than current balance", function () {
            it("rejects the transaction", async () => {
                const balance = await token.balanceOf(accounts[1]);
                const stakeAmount = toWei(balance.add(toBN(toWei("0.01"))));

                await expectRevert(
                    token.transferAndCall(staking.address, stakeAmount, { from: accounts[1] }),
                    "ERC20: transfer amount exceeds balance"
                );
            });
        });

        describe("with an invalid amount", function () {
            it("rejects or allows the transaction depending on the amount", async () => {
                // Stake with an invalid amount
                await expectRevert(
                    token.transferAndCall(staking.address, toWei("0"), { from: accounts[1] }),
                    "ExenoTokenStaking: amount cannot be zero"
                );
                // Stake with a non-zero amount
                await token.transferAndCall(staking.address, toWei("0.0001"), { from: accounts[1] });
                console.log(formatStatus(await staking.getStakingStatus(accounts[1])));
            });
        });
    });

    describe("unstaking", function () {
        describe("single transaction", function () {
            it("succeeds & emits event", async () => {
                const stakeAmount = toWei("100.3278");
                const unstakeAmount = toWei("49.4302");

                await token.transferAndCall(staking.address, stakeAmount, { from: accounts[1] });

                console.log(formatStatus(await staking.getStakingStatus(accounts[1])));

                await advanceTimeAndBlock(await slippage(accounts[1]) + period.hours(28));

                console.log(formatStatus(await staking.getStakingStatus(accounts[1])));

                const receipt1 = await staking.unstake(unstakeAmount, 0, { from: accounts[1] });
                await expectEvent.inTransaction(receipt1.tx, staking, "Unstaked", {
                    user: accounts[1],
                    amount: unstakeAmount
                });

                console.log(formatStatus(await staking.getStakingStatus(accounts[1])));

                (await staking.totalTokensStaked()).should.be.bignumber.equal(toBN(stakeAmount).sub(toBN(unstakeAmount)));
                (await staking.getNumberOfStakeholders()).should.be.bignumber.equal(toBN("1"));

                const receipt2 = await staking.unstake(0, 0, { from: accounts[1] });
                await expectEvent.inTransaction(receipt2.tx, staking, "Unstaked", {
                    user: accounts[1],
                    amount: toBN(stakeAmount).sub(toBN(unstakeAmount))
                });

                console.log(formatStatus(await staking.getStakingStatus(accounts[1])));
            });

            it("unstakes everything when amount is zero", async () => {
                const stakeAmount = toWei("11.4456");

                await token.transferAndCall(staking.address, stakeAmount, { from: accounts[1] });

                console.log(formatStatus(await staking.getStakingStatus(accounts[1])));

                await advanceTimeAndBlock(await slippage(accounts[1]) + period.hours(28));

                const receipt = await staking.unstake(0, 0, { from: accounts[1] });
                await expectEvent.inTransaction(receipt.tx, staking, "Unstaked", {
                    user: accounts[1],
                    amount: stakeAmount
                });

                (await staking.totalTokensStaked()).should.be.bignumber.equal(toBN("0"));
                (await staking.getNumberOfStakeholders()).should.be.bignumber.equal(toBN("1"));
            });

            it("tracks the balance", async () => {
                const stakeAmount = toWei("100.3278");
                const unstakeAmount = toWei("49.4302");

                const totalSupply = await token.totalSupply();

                (await staking.owner()).should.equal(accounts[0]);
                const ownerBalance1 = await token.balanceOf(accounts[0]);

                const stakerBalance1 = await token.balanceOf(accounts[1]);
                stakerBalance1.should.be.bignumber.equal(toBN(toWei("5000")));

                const contractBalance1 = await token.balanceOf(staking.address);
                contractBalance1.should.be.bignumber.equal(toBN("0"));

                // Make two subsequent stakes
                await token.transferAndCall(staking.address, stakeAmount, { from: accounts[1] });
                await token.transferAndCall(staking.address, stakeAmount, { from: accounts[1] });

                console.log(formatStatus(await staking.getStakingStatus(accounts[1])));

                const ownerBalance2 = await token.balanceOf(accounts[0]);
                const stakerBalance2 = await token.balanceOf(accounts[1]);
                const contractBalance2 = await token.balanceOf(staking.address);

                ownerBalance2.should.be.bignumber.equal(ownerBalance1);
                stakerBalance2.should.be.bignumber.equal(stakerBalance1.sub(toBN(stakeAmount)).sub(toBN(stakeAmount)));
                contractBalance2.should.be.bignumber.equal(contractBalance1.add(toBN(stakeAmount)).add(toBN(stakeAmount)));

                await advanceTimeAndBlock(await slippage(accounts[1]) + period.hours(28));

                // Unstake from the first index
                const receipt = await staking.unstake(unstakeAmount, 0, { from: accounts[1] });

                const status = await staking.getStakingStatus(accounts[1]);
                console.log(formatStatus(status));

                status.totalAmount.should.be.bignumber.equal(toBN(stakeAmount).add(toBN(stakeAmount)).sub(toBN(unstakeAmount)));
                status.stakes[0].amount.should.be.bignumber.equal(toBN(stakeAmount).sub(toBN(unstakeAmount)));
                status.stakes[1].amount.should.be.bignumber.equal(toBN(stakeAmount));

                const ownerBalance3 = await token.balanceOf(accounts[0]);
                const stakerBalance3 = await token.balanceOf(accounts[1]);
                const contractBalance3 = await token.balanceOf(staking.address);

                const payout = ownerBalance2.sub(ownerBalance3);

                ownerBalance3.should.be.bignumber.equal(ownerBalance2.sub(payout));
                stakerBalance3.should.be.bignumber.equal(stakerBalance2.add(toBN(unstakeAmount)).add(payout));
                contractBalance3.should.be.bignumber.equal(contractBalance2.sub(toBN(unstakeAmount)));

                await expectEvent.inTransaction(receipt.tx, staking, "Unstaked", {
                    user: accounts[1],
                    amount: unstakeAmount,
                    payout: payout
                });

                // Ensure the token's total supply is uneffected
                (await token.totalSupply()).should.be.bignumber.equal(totalSupply);
            });

            it("unaffected by pausing", async () => {
                const stakeAmount = toWei("12.3255");
                const unstakeAmount = toWei("9.4256");

                await token.transferAndCall(staking.address, stakeAmount, { from: accounts[1] });

                console.log(formatStatus(await staking.getStakingStatus(accounts[1])));

                await advanceTimeAndBlock(await slippage(accounts[1]) + period.hours(44));

                await staking.pause({ from: accounts[0] });
                (await staking.paused()).should.equal(true);

                await staking.unstake(unstakeAmount, 0, { from: accounts[1] });
            });

            it("removes stake if it becomes empty", async () => {
                await token.transferAndCall(staking.address, toWei("100.0000"), { from: accounts[1] });
                await token.transferAndCall(staking.address, toWei("120.8213"), { from: accounts[1] });

                await advanceTimeAndBlock(await slippage(accounts[1]) + period.hours(25));
                await staking.unstake(toWei("80.5000"), 0, { from: accounts[1] });
                await advanceTimeAndBlock(period.hours(3));
                await staking.unstake(toWei("19.5000"), 0, { from: accounts[1] });
                await staking.unstake(toWei("20.8213"), 1, { from: accounts[1] });

                // Ensure the first stake is empty while the second is not
                const status = await staking.getStakingStatus(accounts[1]);
                console.log(formatStatus(status));

                status.stakes[0].user.should.equal(constants.ZERO_ADDRESS);
                status.stakes[1].user.should.equal(accounts[1]);
            });
        });

        describe("multiple unstakings from the same account", function () {
            it("all succeed & emit events", async () => {
                const stakeAmount = toWei("100.3278");
                const unstakeAmount = toWei("49.4302");

                // Make two subsequent stakes from the same account
                await token.transferAndCall(staking.address, stakeAmount, { from: accounts[1] });
                await token.transferAndCall(staking.address, stakeAmount, { from: accounts[1] });

                console.log(formatStatus(await staking.getStakingStatus(accounts[1])));

                await advanceTimeAndBlock(await slippage(accounts[1]) + period.hours(28));

                // Unstake from the first index
                const receipt1 = await staking.unstake(unstakeAmount, 0, { from: accounts[1] });

                await expectEvent.inTransaction(receipt1.tx, staking, "Unstaked", {
                    user: accounts[1],
                    amount: unstakeAmount
                });

                // Unstake again from the first index
                const receipt2 = await staking.unstake(unstakeAmount, 0, { from: accounts[1] });

                await expectEvent.inTransaction(receipt2.tx, staking, "Unstaked", {
                    user: accounts[1],
                    amount: unstakeAmount
                });

                // Unstake from the second index
                const receipt3 = await staking.unstake(unstakeAmount, 1, { from: accounts[1] });

                await expectEvent.inTransaction(receipt3.tx, staking, "Unstaked", {
                    user: accounts[1],
                    amount: unstakeAmount
                });

                const status = await staking.getStakingStatus(accounts[1]);
                console.log(formatStatus(status));

                status.totalAmount.should.be.bignumber.equal(toBN(stakeAmount).add(toBN(stakeAmount))
                    .sub(toBN(unstakeAmount)).sub(toBN(unstakeAmount)).sub(toBN(unstakeAmount)));
                status.stakes[0].amount.should.be.bignumber.equal(toBN(stakeAmount).sub(toBN(unstakeAmount)).sub(toBN(unstakeAmount)));
                status.stakes[1].amount.should.be.bignumber.equal(toBN(stakeAmount).sub(toBN(unstakeAmount)));

                (await staking.totalTokensStaked()).should.be.bignumber.equal(status.totalAmount);
                (await staking.getNumberOfStakeholders()).should.be.bignumber.equal(toBN("1"));
            });
        });

        describe("multiple unstakings from different accounts", function () {
            it("all succeed & emit events", async () => {
                const stakeAmount = toWei("100.3278");
                const unstakeAmount = toWei("49.4302");

                // Make two subsequent stakes from different accounts
                await token.transferAndCall(staking.address, stakeAmount, { from: accounts[1] });
                await token.transferAndCall(staking.address, stakeAmount, { from: accounts[2] });

                await advanceTimeAndBlock(await slippage(accounts[1]) + period.hours(49));

                // Unstake from the first account
                const receipt1 = await staking.unstake(unstakeAmount, 0, { from: accounts[1] });

                await expectEvent.inTransaction(receipt1.tx, staking, "Unstaked", {
                    user: accounts[1],
                    amount: unstakeAmount
                });

                // Unstake from the second account
                const receipt2 = await staking.unstake(unstakeAmount, 0, { from: accounts[2] });

                await expectEvent.inTransaction(receipt2.tx, staking, "Unstaked", {
                    user: accounts[2],
                    amount: unstakeAmount
                });

                (await staking.totalTokensStaked()).should.be.bignumber.equal(toBN(stakeAmount).add(toBN(stakeAmount)).sub(toBN(unstakeAmount)).sub(toBN(unstakeAmount)));
                (await staking.getNumberOfStakeholders()).should.be.bignumber.equal(toBN("2"));
            });
        });

        describe("when there are not enough funds for rewards", function () {
            describe("due to a low balance on the owner's account", function () {
                it("rejects the transaction but then allows it when owner's balance is refilled", async () => {
                    (await staking.owner()).should.equal(accounts[0]);

                    // The owner transfers everything but 1.00 to some other account
                    const ownerBalance = await token.balanceOf(accounts[0]);
                    await token.transfer(accounts[9], ownerBalance.sub(toBN(toWei("1.00"))), { from: accounts[0] });
                    (await token.balanceOf(accounts[0])).should.be.bignumber.equal(toBN(toWei("1.00")));

                    const amount = toWei("3000.00");

                    await token.transferAndCall(staking.address, amount, { from: accounts[1] });

                    await advanceTimeAndBlock(await slippage(accounts[1]) + period.hours(28));

                    const status = await staking.getStakingStatus(accounts[1]);
                    console.log(formatStatus(status));

                    // Ensure the owner's balance is less than the claimable amount
                    (await token.balanceOf(accounts[0])).should.be.bignumber.lessThan(status.stakes[0].claimable);

                    await expectRevert(
                        staking.unstake(amount, 0, { from: accounts[1] }),
                        "ExenoTokenStaking: not enough balance for payout"
                    );

                    // The owner's balance is refilled
                    await token.transfer(accounts[0], toWei("1.00"), { from: accounts[2] });

                    // Ensure the owner's balance is now greater than the claimable amount
                    (await token.balanceOf(accounts[0])).should.be.bignumber.greaterThan(status.stakes[0].claimable);

                    // And now unstaking should work
                    await staking.unstake(amount, 0, { from: accounts[1] });
                });

                it("rejects the transaction but then allows it when the stakeholder gives up on payout", async () => {
                    (await staking.owner()).should.equal(accounts[0]);

                    // The owner transfers everything but 1.00 to some other account
                    const ownerBalance = await token.balanceOf(accounts[0]);
                    await token.transfer(accounts[9], ownerBalance.sub(toBN(toWei("1.00"))), { from: accounts[0] });
                    (await token.balanceOf(accounts[0])).should.be.bignumber.equal(toBN(toWei("1.00")));

                    const amount = toWei("3000.00");

                    await token.transferAndCall(staking.address, amount, { from: accounts[1] });

                    await advanceTimeAndBlock(await slippage(accounts[1]) + + period.hours(28));

                    const status = await staking.getStakingStatus(accounts[1]);
                    console.log(formatStatus(status));

                    // Ensure the owner's balance is less than the claimable amount
                    (await token.balanceOf(accounts[0])).should.be.bignumber.lessThan(status.stakes[0].claimable);

                    await expectRevert(
                        staking.unstake(amount, 0, { from: accounts[1] }),
                        "ExenoTokenStaking: not enough balance for payout"
                    );

                    // Yet unstaking should be possible without payout
                    const receipt = await staking.unstakeWithoutPayout(amount, 0, { from: accounts[1] });
                    await expectEvent.inTransaction(receipt.tx, staking, "Unstaked", {
                        user: accounts[1],
                        amount: amount,
                        payout: toWei("0")
                    });
                });
            });

            describe("due to the owner reducing spending allowance", function () {
                it("rejects the transaction but then allows it when allowance is reinstated", async () => {
                    (await staking.owner()).should.equal(accounts[0]);

                    // The owner reduces allowance to 1.00
                    await token.approve(staking.address, toWei("1.00"), { from: accounts[0] });
                    (await token.allowance(accounts[0], staking.address)).should.be.bignumber.equal(toBN(toWei("1.00")));

                    const amount = toWei("3000.00");

                    await token.transferAndCall(staking.address, amount, { from: accounts[1] });

                    await advanceTimeAndBlock(await slippage(accounts[1]) + period.hours(28));

                    const status = await staking.getStakingStatus(accounts[1]);
                    console.log(formatStatus(status));

                    // Ensure the owner's balance is less than the claimable amount
                    (await token.allowance(accounts[0], staking.address)).should.be.bignumber.lessThan(status.stakes[0].claimable);

                    await expectRevert(
                        staking.unstake(amount, 0, { from: accounts[1] }),
                        "ExenoTokenStaking: not enough allowance for payout"
                    );

                    // The owner increases allowance to 2.00
                    await token.approve(staking.address, toWei("2.00"), { from: accounts[0] });

                    // Ensure the owner's balance is now greater than the claimable amount
                    (await token.allowance(accounts[0], staking.address)).should.be.bignumber.greaterThan(status.stakes[0].claimable);

                    // And now unstaking should work
                    await staking.unstake(amount, 0, { from: accounts[1] });
                });

                it("rejects the transaction but then allows it when the stakeholder gives up on payout", async () => {
                    (await staking.owner()).should.equal(accounts[0]);

                    // The owner reduces allowance to 1.00
                    await token.approve(staking.address, toWei("1.00"), { from: accounts[0] });
                    (await token.allowance(accounts[0], staking.address)).should.be.bignumber.equal(toBN(toWei("1.00")));

                    const amount = toWei("3000.00");

                    await token.transferAndCall(staking.address, amount, { from: accounts[1] });

                    await advanceTimeAndBlock(await slippage(accounts[1]) + period.hours(28));

                    const status = await staking.getStakingStatus(accounts[1]);
                    console.log(formatStatus(status));

                    // Ensure the owner's balance is less than the claimable amount
                    (await token.allowance(accounts[0], staking.address)).should.be.bignumber.lessThan(status.stakes[0].claimable);

                    await expectRevert(
                        staking.unstake(amount, 0, { from: accounts[1] }),
                        "ExenoTokenStaking: not enough allowance for payout"
                    );

                    // Yet unstaking should be possible without payout
                    const receipt = await staking.unstakeWithoutPayout(amount, 0, { from: accounts[1] });
                    await expectEvent.inTransaction(receipt.tx, staking, "Unstaked", {
                        user: accounts[1],
                        amount: amount,
                        payout: toWei("0")
                    });
                });
            });
        });

        describe("when the requested amount is more than what is deposited at an index", function () {
            it("rejects or allows the transaction depending on the index", async () => {
                const stakeAmount1 = toWei("90.9999");
                const stakeAmount2 = toWei("10.1234");
                const unstakeAmount = toBN(stakeAmount2).add(toBN(toWei("0.0001")));

                await token.transferAndCall(staking.address, stakeAmount1, { from: accounts[1] });
                await token.transferAndCall(staking.address, stakeAmount2, { from: accounts[1] });

                await expectRevert(
                    staking.unstake(unstakeAmount, 1, { from: accounts[1] }),
                    "ExenoTokenStaking: there is not enough stake on the provided index for the requested amount"
                );
                await staking.unstake(unstakeAmount, 0, { from: accounts[1] });
            });
        });

        describe("when an account has had a stake but it's empty now", function () {
            it("rejects the transaction", async () => {
                // Stake and unstake the same amount
                await token.transferAndCall(staking.address, toWei("220"), { from: accounts[1] });
                await advanceTimeAndBlock(await slippage(accounts[1]) + period.hours(29));
                await staking.unstake(toWei("220"), 0, { from: accounts[1] });

                const status = await staking.getStakingStatus(accounts[1]);
                console.log(formatStatus(status));

                status.totalAmount.should.be.bignumber.equal(toBN("0"));

                await expectRevert(
                    staking.unstake(toWei("99"), 0, { from: accounts[1] }),
                    "ExenoTokenStaking: there is not enough stake on the provided index for the requested amount"
                );
            });
        });

        describe("when an account has never had a stake", function () {
            it("rejects the transaction", async () => {
                const status = await staking.getStakingStatus(accounts[9]);
                console.log(formatStatus(status));

                status.totalAmount.should.be.bignumber.equal(toBN("0"));

                await expectRevert(
                    staking.unstake(toWei("100"), 0, { from: accounts[9] }),
                    "ExenoTokenStaking: this account has never made any staking"
                );

                await expectRevert(
                    staking.unstake(0, 0, { from: accounts[9] }),
                    "ExenoTokenStaking: this account has never made any staking"
                );
            });
        });

        describe("when an account is different than the one used for staking", function () {
            it("rejects the transaction and then succeeds when the correct account is used", async () => {
                const stakeAmount = toWei("94.4766");
                const unstakeAmount = toWei("67.4563");

                await token.transferAndCall(staking.address, stakeAmount, { from: accounts[1] });

                console.log(formatStatus(await staking.getStakingStatus(accounts[1])));

                await await advanceTimeAndBlock(await slippage(accounts[1]) + period.hours(28));

                await expectRevert(
                    staking.unstake(unstakeAmount, 0, { from: accounts[2] }),
                    "ExenoTokenStaking: this account has never made any staking"
                );

                const receipt = await staking.unstake(unstakeAmount, 0, { from: accounts[1] });
                await expectEvent.inTransaction(receipt.tx, staking, "Unstaked", {
                    user: accounts[1],
                    amount: unstakeAmount
                });
            });
        });

        describe("when an invalid index is supplied", function () {
            it("rejects or allows the transaction depending on the index", async () => {
                await token.transferAndCall(staking.address, toWei("99"), { from: accounts[1] });
                await token.transferAndCall(staking.address, toWei("21"), { from: accounts[1] });
                console.log(formatStatus(await staking.getStakingStatus(accounts[1])));

                await await advanceTimeAndBlock(await slippage(accounts[1]) + period.hours(28));

                // Unstake with an invalid index
                await expectRevert(
                    staking.unstake(toWei("90"), 2, { from: accounts[1] }),
                    "ExenoTokenStaking: provided index is out of range"
                );
                // Unstake with a valid index
                await staking.unstake(toWei("90"), 0, { from: accounts[1] });
                console.log(formatStatus(await staking.getStakingStatus(accounts[1])));
            });
        });

        describe("unstaking before the first rebase", function () {
            describe("done immediately", function () {
                it("allows the transaction with zero payout", async () => {
                    const stakeAmount = toWei("10.3278");
                    const unstakeAmount = toWei("9.4302");

                    await token.transferAndCall(staking.address, stakeAmount, { from: accounts[1] });
                    console.log(formatStatus(await staking.getStakingStatus(accounts[1])));

                    const receipt = await staking.unstake(unstakeAmount, 0, { from: accounts[1] });
                    await expectEvent.inTransaction(receipt.tx, staking, "Unstaked", {
                        user: accounts[1],
                        amount: unstakeAmount,
                        payout: toWei("0")
                    });
                    console.log(formatStatus(await staking.getStakingStatus(accounts[1])));
                });
            });

            describe("after a few minutes", function () {
                it("allows the transaction without any payout", async () => {
                    const stakeAmount = toWei("8.5847");
                    const unstakeAmount = toWei("6.6887");

                    await token.transferAndCall(staking.address, stakeAmount, { from: accounts[1] });
                    console.log(formatStatus(await staking.getStakingStatus(accounts[1])));

                    await advanceTimeAndBlock(await slippage(accounts[1]) + period.minutes(13));

                    const receipt = await staking.unstake(unstakeAmount, 0, { from: accounts[1] });
                    await expectEvent.inTransaction(receipt.tx, staking, "Unstaked", {
                        user: accounts[1],
                        amount: unstakeAmount,
                        payout: toWei("0")
                    });
                    console.log(formatStatus(await staking.getStakingStatus(accounts[1])));
                });
            });
        });
    });
});