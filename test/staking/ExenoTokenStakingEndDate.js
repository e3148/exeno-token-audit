const { expectEvent, expectRevert } = require("@openzeppelin/test-helpers");
const { toWei, fromWei, toBN } = require("web3-utils");
const { advanceTimeAndBlock, getBlock, period } = require("../../utils/time");
const { formatStatus, formatTimestamp } = require("../../utils/format");

require('chai').use(require('chai-as-promised')).should();

const ExenoToken = artifacts.require("ExenoToken");
const ExenoTokenStaking = artifacts.require("ExenoTokenStaking");

contract("ExenoTokenStakingEndDate", async (accounts) => {
    let token;
    let staking;
    let block;
    let endDate;

    beforeEach(async () => {
        block = await getBlock();
        endDate = block.timestamp + period.days(1);

        token = await ExenoToken.new();
        staking = await ExenoTokenStaking.new(
            token.address, 876, 24, 0, endDate);

        await token.approve(staking.address, toWei("1000"));
        await token.transfer(accounts[1], toWei("1000"));
        await token.transfer(accounts[2], toWei("1000"));
    });

    it("setting end-date", async () => {
        const newEndDate = endDate + period.hours(1);
        await staking.extendEndDate(newEndDate);
        (await staking.endDate()).should.be.bignumber.equal(toBN(newEndDate));
    });

    it("new end-date in the past", async () => {
        console.log(formatTimestamp(await staking.endDate()));
        console.log(formatTimestamp(block.timestamp));

        await advanceTimeAndBlock(endDate - block.timestamp + period.days(1));

        block = await getBlock();
        console.log(formatTimestamp(block.timestamp));

        await expectRevert(
            staking.extendEndDate(block.timestamp - period.hours(1)),
            "ExenoTokenStaking: new end-date cannot be in the past"
        );

        await expectRevert(
            staking.extendEndDate(block.timestamp),
            "ExenoTokenStaking: new end-date cannot be in the past"
        );
    });

    it("existing end-date is expired", async () => {
        console.log(formatTimestamp(await staking.endDate()));
        console.log(formatTimestamp(block.timestamp));

        await advanceTimeAndBlock(endDate - block.timestamp + period.days(1));

        block = await getBlock();
        console.log(formatTimestamp(block.timestamp));

        await expectRevert(
            staking.extendEndDate(block.timestamp + period.hours(1)),
            "ExenoTokenStaking: cannot set a new end-date as existing end-date is expired"
        );
    });

    it("new end-date is prior to existing end-date", async () => {
        console.log(formatTimestamp(await staking.endDate()));

        await expectRevert(
            staking.extendEndDate(endDate - period.minutes(1)),
            "ExenoTokenStaking: new end-date cannot be prior to existing end-date"
        );
        await expectRevert(
            staking.extendEndDate(endDate),
            "ExenoTokenStaking: new end-date cannot be prior to existing end-date"
        );
        await staking.extendEndDate(endDate + period.minutes(1));
    });

    it("only owner can set end-date", async () => {
        console.log(formatTimestamp(await staking.endDate()));

        await expectRevert(
            staking.extendEndDate(endDate + period.minutes(1), { from: accounts[1] }),
            "Ownable: caller is not the owner"
        );
        await staking.extendEndDate(endDate + period.minutes(1), { from: accounts[0] });
    });

    it("staking after end-date", async () => {
        console.log(formatTimestamp(await staking.endDate()));
        console.log(formatTimestamp(block.timestamp));

        await advanceTimeAndBlock(endDate - block.timestamp + period.days(1));

        block = await getBlock();
        console.log(formatTimestamp(block.timestamp));

        await expectRevert(
            token.transferAndCall(staking.address, toWei("100"), { from: accounts[1] }),
            "ExenoTokenStaking: end-date is expired"
        );
    });

    it("executing payouts with end-date within time horizon", async () => {
        await staking.extendEndDate(block.timestamp + period.hours(30));
        console.log(formatTimestamp(await staking.endDate()));

        const stakingAmount = toWei("255.17");
        const unstakingAmount1 = toWei("112.85");
        const unstakingAmount2 = toWei("71.16");
        const unstakingAmount3 = toWei("71.16");

        const duration1 = period.hours(25);
        const duration2 = period.hours(30);
        const duration3 = period.hours(125);

        const expectedInterest1A = toWei("0.0637925");
        const expectedClaimable1A = toWei("0.0612408");
        const expectedInterest1B = toWei("0.03558");
        const expectedClaimable1B = toWei("0.0341568");

        const expectedInterest2A = toWei("0.0683136");
        const expectedClaimable2A = toWei("0.0683136");
        const expectedInterest2B = toWei("0.0341568");
        const expectedClaimable2B = toWei("0.0341568");

        const expectedInterest3A = toWei("0.0341568");
        const expectedClaimable3A = toWei("0.0341568");
        const expectedInterest3B = toWei("0");
        const expectedClaimable3B = toWei("0");

        const expectedPayout1 = toWei("0.027084");
        const expectedPayout2 = toWei("0.0341568");
        const expectedPayout3 = toWei("0.0341568");

        const initialStakerBalance = await token.balanceOf(accounts[1]);


        await token.transferAndCall(staking.address, stakingAmount, { from: accounts[1] });

        let status = await staking.getStakingStatus(accounts[1]);
        console.log(formatStatus(status));


        await advanceTimeAndBlock(duration1);


        status = await staking.getStakingStatus(accounts[1]);
        console.log(formatStatus(status));

        assert.equal(fromWei(status.stakes[0].interest), fromWei(expectedInterest1A));
        assert.equal(fromWei(status.stakes[0].claimable), fromWei(expectedClaimable1A));

        await staking.unstake(unstakingAmount1, 0, { from: accounts[1] });

        status = await staking.getStakingStatus(accounts[1]);
        console.log(formatStatus(status));

        assert.equal(fromWei(status.stakes[0].interest), fromWei(expectedInterest1B));
        assert.equal(fromWei(status.stakes[0].claimable), fromWei(expectedClaimable1B));

        const newStakerBalance1 = await token.balanceOf(accounts[1]);
        const expectedStakerBalance1 = toBN(initialStakerBalance)
            .sub(toBN(stakingAmount))
            .add(toBN(unstakingAmount1))
            .add(toBN(expectedPayout1));

        // Ensure payout is paid to the staker
        assert.equal(fromWei(newStakerBalance1), fromWei(expectedStakerBalance1));


        await advanceTimeAndBlock(duration2);


        status = await staking.getStakingStatus(accounts[1]);
        console.log(formatStatus(status));

        assert.equal(fromWei(status.stakes[0].interest), fromWei(expectedInterest2A));
        assert.equal(fromWei(status.stakes[0].claimable), fromWei(expectedClaimable2A));

        await staking.unstake(unstakingAmount2, 0, { from: accounts[1] });

        status = await staking.getStakingStatus(accounts[1]);
        console.log(formatStatus(status));

        assert.equal(fromWei(status.stakes[0].interest), fromWei(expectedInterest2B));
        assert.equal(fromWei(status.stakes[0].claimable), fromWei(expectedClaimable2B));

        const newStakerBalance2 = await token.balanceOf(accounts[1]);
        const expectedStakerBalance2 = toBN(newStakerBalance1)
            .add(toBN(unstakingAmount2))
            .add(toBN(expectedPayout2));

        // Ensure payout is paid to the staker
        assert.equal(fromWei(newStakerBalance2), fromWei(expectedStakerBalance2));


        await advanceTimeAndBlock(duration3);


        status = await staking.getStakingStatus(accounts[1]);
        console.log(formatStatus(status));

        assert.equal(fromWei(status.stakes[0].interest), fromWei(expectedInterest3A));
        assert.equal(fromWei(status.stakes[0].claimable), fromWei(expectedClaimable3A));

        await staking.unstake(unstakingAmount3, 0, { from: accounts[1] });

        status = await staking.getStakingStatus(accounts[1]);
        console.log(formatStatus(status));

        assert.equal(fromWei(status.stakes[0].interest), fromWei(expectedInterest3B));
        assert.equal(fromWei(status.stakes[0].claimable), fromWei(expectedClaimable3B));

        const newStakerBalance3 = await token.balanceOf(accounts[1]);
        const expectedStakerBalance3 = toBN(newStakerBalance2)
            .add(toBN(unstakingAmount3))
            .add(toBN(expectedPayout3));

        // Ensure no payout has been paid
        assert.equal(fromWei(newStakerBalance3), fromWei(expectedStakerBalance3));
    });

    it("executing payouts with end-date beyond time horizon", async () => {
        await staking.extendEndDate(block.timestamp + period.hours(300));
        console.log(formatTimestamp(await staking.endDate()));

        const stakingAmount = toWei("255.17");
        const unstakingAmount1 = toWei("112.85");
        const unstakingAmount2 = toWei("71.16");
        const unstakingAmount3 = toWei("71.16");

        const duration1 = period.hours(25);
        const duration2 = period.hours(30);
        const duration3 = period.hours(125);

        const expectedInterest1A = toWei("0.0637925");
        const expectedClaimable1A = toWei("0.0612408");
        const expectedInterest1B = toWei("0.03558");
        const expectedClaimable1B = toWei("0.0341568");

        const expectedInterest2A = toWei("0.0782760");
        const expectedClaimable2A = toWei("0.0683136");
        const expectedInterest2B = toWei("0.0391380");
        const expectedClaimable2B = toWei("0.0341568");

        const expectedInterest3A = toWei("0.1280880");
        const expectedClaimable3A = toWei("0.1195488");
        const expectedInterest3B = toWei("0");
        const expectedClaimable3B = toWei("0");

        const expectedPayout1 = toWei("0.027084");
        const expectedPayout2 = toWei("0.0341568");
        const expectedPayout3 = toWei("0.1195488");

        const initialStakerBalance = await token.balanceOf(accounts[2]);


        await token.transferAndCall(staking.address, stakingAmount, { from: accounts[2] });

        let status = await staking.getStakingStatus(accounts[2]);
        console.log(formatStatus(status));


        await advanceTimeAndBlock(duration1);


        status = await staking.getStakingStatus(accounts[2]);
        console.log(formatStatus(status));

        assert.equal(fromWei(status.stakes[0].interest), fromWei(expectedInterest1A));
        assert.equal(fromWei(status.stakes[0].claimable), fromWei(expectedClaimable1A));

        await staking.unstake(unstakingAmount1, 0, { from: accounts[2] });

        status = await staking.getStakingStatus(accounts[2]);
        console.log(formatStatus(status));

        assert.equal(fromWei(status.stakes[0].interest), fromWei(expectedInterest1B));
        assert.equal(fromWei(status.stakes[0].claimable), fromWei(expectedClaimable1B));

        const newStakerBalance1 = await token.balanceOf(accounts[2]);
        const expectedStakerBalance1 = toBN(initialStakerBalance)
            .sub(toBN(stakingAmount))
            .add(toBN(unstakingAmount1))
            .add(toBN(expectedPayout1));
        // Ensure payout is paid to the staker
        assert.equal(fromWei(newStakerBalance1), fromWei(expectedStakerBalance1));


        await advanceTimeAndBlock(duration2);


        status = await staking.getStakingStatus(accounts[2]);
        console.log(formatStatus(status));

        assert.equal(fromWei(status.stakes[0].interest), fromWei(expectedInterest2A));
        assert.equal(fromWei(status.stakes[0].claimable), fromWei(expectedClaimable2A));

        await staking.unstake(unstakingAmount2, 0, { from: accounts[2] });

        status = await staking.getStakingStatus(accounts[2]);
        console.log(formatStatus(status));

        assert.equal(fromWei(status.stakes[0].interest), fromWei(expectedInterest2B));
        assert.equal(fromWei(status.stakes[0].claimable), fromWei(expectedClaimable2B));

        const newStakerBalance2 = await token.balanceOf(accounts[2]);
        const expectedStakerBalance2 = toBN(newStakerBalance1)
            .add(toBN(unstakingAmount2))
            .add(toBN(expectedPayout2));
        // Ensure payout is paid to the staker
        assert.equal(fromWei(newStakerBalance2), fromWei(expectedStakerBalance2));


        await advanceTimeAndBlock(duration3);


        status = await staking.getStakingStatus(accounts[2]);
        console.log(formatStatus(status));

        assert.equal(fromWei(status.stakes[0].interest), fromWei(expectedInterest3A));
        assert.equal(fromWei(status.stakes[0].claimable), fromWei(expectedClaimable3A));

        await staking.unstake(unstakingAmount3, 0, { from: accounts[2] });

        status = await staking.getStakingStatus(accounts[2]);
        console.log(formatStatus(status));

        assert.equal(fromWei(status.stakes[0].interest), fromWei(expectedInterest3B));
        assert.equal(fromWei(status.stakes[0].claimable), fromWei(expectedClaimable3B));

        const newStakerBalance3 = await token.balanceOf(accounts[2]);
        const expectedStakerBalance3 = toBN(newStakerBalance2)
            .add(toBN(unstakingAmount3))
            .add(toBN(expectedPayout3));
        // Ensure no payout has been paid
        assert.equal(fromWei(newStakerBalance3), fromWei(expectedStakerBalance3));
    });
});