const { constants, expectRevert, expectEvent } = require("@openzeppelin/test-helpers");
const { toWei, fromWei, toBN } = require("web3-utils");
const { advanceBlock, advanceTimeAndBlock, getBlock, period } = require("../../utils/time");
const { formatNumber } = require("../../utils/format");
const { Converter } = require("../../utils/converter");

require('chai').use(require('chai-as-promised')).should();

const ExenoToken = artifacts.require('ExenoToken');
const PriceFeed = artifacts.require('PriceFeed');
const ExenoTokenIco = artifacts.require('ExenoTokenIco');

contract('ExenoTokenIco', (accounts) => {
    const wallet = accounts[1];
    const nonOwner = accounts[5];
    const investor1 = accounts[6];
    const investor2 = accounts[7];

    const price = 166260638;
    const decimals = 8;

    const allowance = 50 * 1000 * 1000;

    const preIcoStage = 0;
    const icoStage = 1;
    const postIcoStage = 2;

    const preIcoRate = 3500;
    const icoRate = 5000;

    const minCap = toWei("1");
    const maxCap = toWei("100000");

    const startDate = Math.round(Date.parse("2021-01-01T00:00:00+0000") / 1000);

    const getTokenAmountFromCashAmount = function (cashAmount, price, decimals, rate) {
        const converter = new Converter(price, decimals, rate);
        const { tokenAmount } = converter.convertFromCashAmount(cashAmount);
        return tokenAmount;
    }

    const getCashAmountFromTokenAmount = function (tokenAmount, price, decimals, rate) {
        const converter = new Converter(price, decimals, rate);
        const { cashAmount } = converter.convertFromTokenAmount(tokenAmount);
        return cashAmount;
    }

    let block;
    let token;
    let priceFeed;
    let ico;

    before(async () => {
        await advanceBlock();
    });

    beforeEach(async () => {
        block = await getBlock();
        token = await ExenoToken.new();
        priceFeed = await PriceFeed.new(price, decimals);
        ico = await ExenoTokenIco.new(
            token.address,
            priceFeed.address,
            preIcoRate,
            icoRate,
            minCap,
            maxCap,
            wallet,
            startDate
        );
    });

    describe('contract creation', function () {
        it('rejects when wallet address is invalid', async function () {
            await expectRevert(
                ExenoTokenIco.new(
                    token.address,
                    priceFeed.address,
                    preIcoRate,
                    icoRate,
                    minCap,
                    maxCap,
                    constants.ZERO_ADDRESS,
                    startDate),
                "ExenoTokenIco: invalid address"
            );
        });
        it('rejects when rates are invalid', async function () {
            await expectRevert(
                ExenoTokenIco.new(
                    token.address,
                    priceFeed.address,
                    101,
                    99,
                    minCap,
                    maxCap,
                    wallet,
                    startDate),
                "ExenoTokenIco: preICO rate needs to be lower than ICO rate"
            );
        });
        it('rejects when caps are invalid', async function () {
            await expectRevert(
                ExenoTokenIco.new(
                    token.address,
                    priceFeed.address,
                    preIcoRate,
                    icoRate,
                    toWei("101"),
                    toWei("99"),
                    wallet,
                    startDate),
                "ExenoTokenIco: min cap needs to be lower than max cap"
            );
        });
    });

    describe('contract parameters', function () {
        it('tracks associated token', async function () {
            (await ico.token()).should.equal(token.address);
        });
        it('tracks associated priceFeed', async function () {
            (await ico.priceFeed()).should.equal(priceFeed.address);
        });
        it('tracks preIcoRate & icoRate', async function () {
            (await ico.preIcoRate()).should.be.bignumber.equal(toBN(preIcoRate));
            (await ico.icoRate()).should.be.bignumber.equal(toBN(icoRate));
        });
        it('tracks minCap & maxCap', async function () {
            (await ico.minCap()).should.be.bignumber.equal(toBN(minCap));
            (await ico.maxCap()).should.be.bignumber.equal(toBN(maxCap));
        });
        it('tracks wallet', async function () {
            (await ico.wallet()).should.equal(wallet);
        });
        it('tracks startDate', async function () {
            (await ico.startDate()).should.be.bignumber.equal(toBN(startDate));
        });
    });

    describe('contract pausing', async function () {
        it("should be able to be paused and unpaused by the owner", async function () {
            await expectRevert(
                ico.pause({ from: nonOwner }),
                "Ownable: caller is not the owner"
            );
            (await ico.paused()).should.equal(false);

            await ico.pause();
            (await ico.paused()).should.equal(true);

            await expectRevert(
                ico.unpause({ from: nonOwner }),
                "Ownable: caller is not the owner"
            );
            (await ico.paused()).should.equal(true);

            await ico.unpause();
            (await ico.paused()).should.equal(false);
        });
    });

    describe('pre-conditions for buying tokens', function () {
        describe('when balance is not high enough', function () {
            it('does not allow buying tokens', async function () {
                (await ico.owner()).should.equal(accounts[0]);

                // The owner transfers everything but 5.00 to some other account
                const ownerBalance = await token.balanceOf(accounts[0]);
                await token.transfer(accounts[9], ownerBalance.sub(toBN(toWei("5.00"))));
                (await token.balanceOf(accounts[0])).should.be.bignumber.equal(toBN(toWei("5.00")));

                await token.approve(ico.address, toWei(toBN(allowance)));

                const value = toWei("1.7500");
                await expectRevert(
                    ico.buyTokens(investor1, { value: value, from: investor1 }),
                    "ExenoTokenIco: not enough balance on owner's account"
                );

                // The owner's balance is refilled
                await token.transfer(accounts[0], getTokenAmountFromCashAmount(value, price, decimals, preIcoRate).sub(toBN(toWei("5.00"))), { from: accounts[9] });
                (await token.balanceOf(accounts[0])).should.be.bignumber.equal(getTokenAmountFromCashAmount(value, price, decimals, preIcoRate));
                await ico.buyTokens(investor1, { value: value, from: investor1 });
            });
        });

        describe('when no allowance has been made', function () {
            it('does not allow buying tokens', async function () {
                const value = toWei("1.1500");
                await expectRevert(
                    ico.buyTokens(investor1, { value: value, from: investor1 }),
                    "ExenoTokenIco: not enough allowance from owner's account"
                );
            });
        });

        describe('when allowance is made but not high enough', function () {
            it('does not allow buying tokens', async function () {
                await token.approve(ico.address, toWei("5.00"));
                (await token.allowance(accounts[0], ico.address)).should.be.bignumber.equal(toBN(toWei("5.00")));
                const value = toWei("1.7500");
                await expectRevert(
                    ico.buyTokens(investor1, { value: value, from: investor1 }),
                    "ExenoTokenIco: not enough allowance from owner's account"
                );
                await token.approve(ico.address, getTokenAmountFromCashAmount(value, price, decimals, preIcoRate));
                (await token.allowance(accounts[0], ico.address)).should.be.bignumber.equal(getTokenAmountFromCashAmount(value, price, decimals, preIcoRate));
                await ico.buyTokens(investor1, { value: value, from: investor1 });
            });
        });

        describe('when start-date has not come yet', function () {
            it('does not allow buying tokens', async function () {
                token = await ExenoToken.new();
                ico = await ExenoTokenIco.new(
                    token.address,
                    priceFeed.address,
                    preIcoRate,
                    icoRate,
                    minCap,
                    maxCap,
                    wallet,
                    block.timestamp + period.hours(3)
                );
                await token.approve(ico.address, toWei(toBN(allowance)));
                (await ico.currentStage()).should.be.bignumber.equal(toBN(preIcoStage));

                const value = toWei("0.2556");

                await expectRevert(
                    ico.buyTokens(investor1, { value: value, from: investor1 }),
                    "ExenoTokenIco: sale has not started yet"
                );

                await expectRevert(
                    ico.sendTransaction({ value: value, from: investor1 }),
                    "ExenoTokenIco: sale has not started yet"
                );

                await advanceTimeAndBlock(period.hours(3));
                await ico.buyTokens(investor1, { value: value, from: investor1 });
            });
        });
    });

    describe('ico stages', function () {
        beforeEach(async () => {
            await token.approve(ico.address, toWei(toBN(allowance)));
        });

        it('starts in preICO stage', async function () {
            (await ico.currentStage()).should.be.bignumber.equal(toBN(preIcoStage));
        });

        it('allows owner to update the stage to ICO', async function () {
            await ico.nextStage();
            (await ico.currentStage()).should.be.bignumber.equal(toBN(icoStage));
            (await ico.currentRate()).should.be.bignumber.equal(toBN(icoRate));
        });

        it('allows owner to update the stage to postICO', async function () {
            await ico.nextStage();
            await ico.nextStage();
            (await ico.currentStage()).should.be.bignumber.equal(toBN(postIcoStage));
        });

        it('prevents owner from changing the stage when not in preICO or ICO', async function () {
            await ico.nextStage();
            await ico.nextStage();
            (await ico.currentStage()).should.be.bignumber.equal(toBN(postIcoStage));

            await expectRevert(
                ico.nextStage(),
                "ExenoTokenIco: changing stage is only allowed in preICO and ICO"
            );
        });

        it('prevents non-owner from updating the stage', async function () {
            await expectRevert(
                ico.nextStage({ from: nonOwner }),
                "Ownable: caller is not the owner"
            );
            await ico.nextStage();
        });
    });

    describe('updating rates', function () {
        beforeEach(async () => {
            await token.approve(ico.address, toWei(toBN(allowance)));
        });

        it('prevents updating rates in postICO', async function () {
            await ico.nextStage();
            await ico.nextStage();
            (await ico.currentStage()).should.be.bignumber.equal(toBN(postIcoStage));

            const newPreIcoRate = toBN(preIcoRate).add(toBN(1));
            const newIcoRate = toBN(icoRate).add(toBN(5));

            await expectRevert(
                ico.updateRates(newPreIcoRate, newIcoRate),
                "ExenoTokenIco: updating rates is only allowed in preICO and ICO"
            );
        });

        it('allows owner to update rates', async function () {
            const newPreIcoRate = toBN(preIcoRate).add(toBN(1));
            const newIcoRate = toBN(icoRate).add(toBN(5));
            await ico.updateRates(newPreIcoRate, newIcoRate);
            (await ico.preIcoRate()).should.be.bignumber.equal(newPreIcoRate);
            (await ico.icoRate()).should.be.bignumber.equal(newIcoRate);
            (await ico.currentRate()).should.be.bignumber.equal(newPreIcoRate);
        });

        it('prevents non-owner from updating rates', async function () {
            const newPreIcoRate = toBN(preIcoRate).add(toBN(1));
            const newIcoRate = toBN(icoRate).add(toBN(5));
            await expectRevert(
                ico.updateRates(newPreIcoRate, newIcoRate, { from: nonOwner }),
                "Ownable: caller is not the owner"
            );
        });

        it('updated rates are effective', async function () {
            const newPreIcoRate = toBN(preIcoRate).add(toBN(8));
            const newIcoRate = toBN(icoRate).add(toBN(3));

            await ico.updateRates(newPreIcoRate, newIcoRate);
            
            const value1 = toWei("0.2345");
            await ico.buyTokens(investor1, { value: value1, from: investor1 });
            (await ico.tokenPurchases(investor1)).should.be.bignumber.equal(getTokenAmountFromCashAmount(value1, price, decimals, newPreIcoRate));
            
            await ico.nextStage();

            const value2 = toWei("0.3341");
            await ico.sendTransaction({ value: value2, from: investor2 });
            (await ico.tokenPurchases(investor2)).should.be.bignumber.equal(getTokenAmountFromCashAmount(value2, price, decimals, newIcoRate));
        });
    });

    describe('accepting payments & emitting events', function () {
        beforeEach(async () => {
            await token.approve(ico.address, toWei(toBN(allowance)));
        });

        it('accepts payments', async function () {
            const value = toWei("0.2474");
            await ico.sendTransaction({ value: value, from: investor1 });
            await ico.buyTokens(investor1, { value: value, from: investor2 });
        });

        it('allows to buy tokens & emit an event', async function () {
            const value = toWei("0.2182");
            const amount = getTokenAmountFromCashAmount(value, price, decimals, preIcoRate);
            const receipt = await ico.buyTokens(investor1, { value: value, from: investor2 });
            await expectEvent.inTransaction(receipt.tx, ico, "TokenPurchase", {
                purchaser: investor2,
                beneficiary: investor1,
                cashAmount: value,
                tokenAmount: amount
            });
        });

        it('allows to buy tokens via a simple transfer', async function () {
            const value = toWei("0.3213");
            const amount = getTokenAmountFromCashAmount(value, price, decimals, preIcoRate);
            const receipt = await ico.sendTransaction({ value: value, from: investor1 });
            await expectEvent.inTransaction(receipt.tx, ico, "TokenPurchase", {
                purchaser: investor1,
                beneficiary: investor1,
                cashAmount: value,
                tokenAmount: amount
            });
        });
    });

    describe('buying tokens', function () {
        beforeEach(async () => {
            await token.approve(ico.address, toWei(toBN(allowance)));
        });

        describe('when arguments are invalid', function () {
            it('rejects the transaction', async function () {
                const value = toWei("1.4900");
                await expectRevert(
                    ico.buyTokens(constants.ZERO_ADDRESS, { value: value, from: investor1 }),
                    "ExenoTokenIco: invalid address"
                );
                await expectRevert(
                    ico.buyTokens(investor1, { value: toWei("0"), from: investor1 }),
                    "ExenoTokenIco: invalid value"
                );
            });
        });

        describe('when contract is paused', function () {
            it('rejects the transaction', async function () {
                await ico.pause();
                (await ico.paused()).should.equal(true);
                const value = toWei("0.4474");
                await expectRevert(
                    ico.buyTokens(investor1, { value: value, from: investor1 }),
                    "Pausable: paused"
                );
                await expectRevert(
                    ico.sendTransaction({ value: value, from: investor1 }),
                    "Pausable: paused"
                );
            });
        });

        describe('when already in postICO', function () {
            it('rejects the transaction', async function () {
                await ico.nextStage();
                await ico.nextStage();
                (await ico.currentStage()).should.be.bignumber.equal(toBN(postIcoStage));

                const value = toWei("0.2451");
                await expectRevert(
                    ico.buyTokens(investor1, { value: value, from: investor1 }),
                    "ExenoTokenIco: buying tokens is only allowed in preICO and ICO"
                );
                await expectRevert(
                    ico.sendTransaction({ value: value, from: investor1 }),
                    "ExenoTokenIco: buying tokens is only allowed in preICO and ICO"
                );
            });
        });

        describe('when purchase is less than the minimum cap', function () {
            it('rejects the transaction', async function () {
                const minCap = await ico.minCap();
                console.log(formatNumber(fromWei(minCap)));

                const value = getCashAmountFromTokenAmount(minCap, price, decimals, preIcoRate);
                console.log(formatNumber(fromWei(value)));

                await expectRevert(
                    ico.buyTokens(investor2, { value: toBN(value).sub(toBN(toWei("0.0001"))), from: investor2 }),
                    "ExenoTokenIco: purchase is below min cap"
                );

                await expectRevert(
                    ico.sendTransaction({ value: toBN(value).sub(toBN(toWei("0.0001"))), from: investor2 }),
                    "ExenoTokenIco: purchase is below min cap"
                );

                await ico.buyTokens(investor2, { value: toBN(value).add(toBN(toWei("0.0001"))), from: investor2 });
            });
        });

        describe('when investor has already met the minimum cap', function () {
            it('allows the investor to continue contributing up till the maximum cap', async function () {
                let price = 100000;
                let decimals = 0;

                token = await ExenoToken.new();
                priceFeed = await PriceFeed.new(price, decimals);
                ico = await ExenoTokenIco.new(
                    token.address,
                    priceFeed.address,
                    preIcoRate,
                    icoRate,
                    minCap,
                    maxCap,
                    wallet,
                    startDate
                );

                await token.approve(ico.address, toWei(toBN(allowance)));

                const value1 = getCashAmountFromTokenAmount(toBN(minCap).add(toBN(toWei("0.1"))), price, decimals, preIcoRate);
                console.log(formatNumber(fromWei(value1)));
                await ico.buyTokens(investor1, { value: value1, from: investor1 });

                const value2 = getCashAmountFromTokenAmount(toBN(minCap).sub(toBN(toWei("0.1"))), price, decimals, preIcoRate);
                console.log(formatNumber(fromWei(value2)));
                await ico.buyTokens(investor1, { value: value2, from: investor1 });

                const amount = await ico.tokenPurchases(investor1);
                const value3 = getCashAmountFromTokenAmount(toBN(maxCap).sub(toBN(amount)), price, decimals, preIcoRate);
                console.log(formatNumber(fromWei(value3)));
                await ico.buyTokens(investor1, { value: value3, from: investor1 });

                (await ico.tokenPurchases(investor1)).should.be.bignumber.equal(maxCap);
            });

            it('prevents the investor from contributing above the maximum cap', async function () {
                let price = 100000;
                let decimals = 0;

                token = await ExenoToken.new();
                priceFeed = await PriceFeed.new(price, decimals);
                ico = await ExenoTokenIco.new(
                    token.address,
                    priceFeed.address,
                    preIcoRate,
                    icoRate,
                    minCap,
                    maxCap,
                    wallet,
                    startDate
                );

                await token.approve(ico.address, toWei(toBN(allowance)));

                const value1 = getCashAmountFromTokenAmount(toBN(maxCap).sub(toBN(toWei("0.1"))), price, decimals, preIcoRate);
                console.log(formatNumber(fromWei(value1)));
                await ico.buyTokens(investor2, { value: value1, from: investor2 });

                const value2 = getCashAmountFromTokenAmount(toWei("0.1"), price, decimals, preIcoRate);
                console.log(formatNumber(fromWei(value2)));
                await ico.buyTokens(investor2, { value: value2, from: investor2 });

                await expectRevert(
                    ico.buyTokens(investor2, { value: value2, from: investor2 }),
                    "ExenoTokenIco: purchase is above max cap"
                );
            });
        });

        describe('when investor purchased tokens', function () {
            it('checks there is only 1 beneficiary after 1 call of buyTokens()', async function () {
                const value = toWei("0.4567");
                await ico.buyTokens(investor1, { value: value, from: investor1 });
                (await ico.totalBeneficiaries()).should.be.bignumber.equal("1");
            });

            it('checks there are 2 beneficiaries after 2 calls of buyTokens()', async function () {
                const value = toWei("0.2211");
                await ico.buyTokens(investor1, { value: value, from: investor1 });
                await ico.buyTokens(investor2, { value: value, from: investor2 });
                (await ico.totalBeneficiaries()).should.be.bignumber.equal("2");
            });

            it('checks there are 2 beneficiaries after 2 simple transfers', async function () {
                const value = toWei("0.2211");
                await ico.sendTransaction({ value: value, from: investor1 });
                await ico.sendTransaction({ value: value, from: investor2 });
                (await ico.totalBeneficiaries()).should.be.bignumber.equal("2");
            });

            it('checks there are 2 beneficiaries after 1 call of buyTokens() and 1 simple transfer', async function () {
                const value = toWei("0.2211");
                await ico.buyTokens(investor1, { value: value, from: investor1 });
                await ico.sendTransaction({ value: value, from: investor2 });
                (await ico.totalBeneficiaries()).should.be.bignumber.equal("2");
            });

            it('checks there is only 1 beneficiary no matter who the payer is', async function () {
                const value1 = toWei("0.2222");
                await ico.buyTokens(investor1, { value: value1, from: investor1 });

                const value2 = toWei("0.0056");
                await ico.sendTransaction({ value: value2, from: investor1 });

                const value3 = toWei("0.0333");
                await ico.buyTokens(investor1, { value: value3, from: investor2 });

                const value4 = toWei("0.0012");
                await ico.sendTransaction({ value: value4, from: investor1 });

                (await ico.totalBeneficiaries()).should.be.bignumber.equal("1");
            });

            it('checks there is only 1 beneficiary after 2 calls of buyTokens() from the same beneficiary', async function () {
                const value1 = toWei("0.3338");
                await ico.buyTokens(investor1, { value: value1, from: investor1 });

                const value2 = toWei("0.2018");
                await ico.buyTokens(investor1, { value: value2, from: investor1 });
                (await ico.totalBeneficiaries()).should.be.bignumber.equal("1");
            });

            it('checks there is are only 2 beneficiaries after multiple calls of buyTokens() & simple transfers from beneficiary1 and beneficiary2', async function () {
                const value1 = toWei("0.2845");
                await ico.buyTokens(investor1, { value: value1, from: investor1 });
                await ico.sendTransaction({ value: value1, from: investor2 });

                const value2 = toWei("0.0099");
                await ico.sendTransaction({ value: value2, from: investor1 });
                await ico.buyTokens(investor2, { value: value2, from: investor2 });
                await ico.sendTransaction({ value: value2, from: investor2 });

                const value3 = toWei("0.0022");
                await ico.buyTokens(investor2, { value: value3, from: investor1 });
                await ico.buyTokens(investor1, { value: value3, from: investor2 });
                await ico.buyTokens(investor1, { value: value3, from: investor2 });

                (await ico.totalBeneficiaries()).should.be.bignumber.equal("2");
            });
        });

        describe('when purchase is within the valid range', function () {
            it('updates the contribution & purchased amounts when the rate changes', async function () {
                const value1 = toWei("0.5000");
                const tokensPurchased1 = getTokenAmountFromCashAmount(value1, price, decimals, preIcoRate);
                await ico.buyTokens(investor2, { value: value1, from: investor2 });
                (await ico.cashPayments(investor2)).should.be.bignumber.equal(value1);
                (await ico.tokenPurchases(investor2)).should.be.bignumber.equal(tokensPurchased1);

                await ico.nextStage();

                const value2 = toWei("0.1269");
                const tokensPurchased2 = getTokenAmountFromCashAmount(value2, price, decimals, icoRate);
                await ico.buyTokens(investor2, { value: value2, from: investor2 });
                (await ico.cashPayments(investor2)).should.be.bignumber.equal(toBN(value1).add(toBN(value2)));
                (await ico.tokenPurchases(investor2)).should.be.bignumber.equal(tokensPurchased1.add(tokensPurchased2));

                const newIcoRate = toBN(icoRate).add(toBN(3));
                await ico.updateRates(preIcoRate, newIcoRate);
                (await ico.currentRate()).should.be.bignumber.equal(newIcoRate);

                const value3 = toWei("0.0662");
                const tokensPurchased3 = getTokenAmountFromCashAmount(value3, price, decimals, newIcoRate);
                await ico.buyTokens(investor2, { value: value3, from: investor2 });
                (await ico.cashPayments(investor2)).should.be.bignumber.equal(toBN(value1).add(toBN(value2)).add(toBN(value3)));
                (await ico.tokenPurchases(investor2)).should.be.bignumber.equal(tokensPurchased1.add(tokensPurchased2).add(tokensPurchased3));
            });
        });
    });

    describe('token distribution', function () {
        beforeEach(async () => {
            await token.approve(ico.address, toWei(toBN(allowance)));
        });

        it('distributes the correct amount of tokens to investors', async () => {
            const value1 = toWei("0.2974");
            const value2 = toWei("0.3138");

            await ico.buyTokens(investor1, { value: value1, from: investor1 });
            await ico.nextStage();
            await ico.buyTokens(investor2, { value: value2, from: investor2 });
            await ico.nextStage();

            const investor1Amount = getTokenAmountFromCashAmount(value1, price, decimals, preIcoRate);
            const investor2Amount = getTokenAmountFromCashAmount(value2, price, decimals, icoRate);

            (await token.balanceOf(investor1)).should.be.bignumber.equal(investor1Amount);
            (await token.balanceOf(investor2)).should.be.bignumber.equal(investor2Amount);
        });
    });

    describe('withdrawal of funds to the company wallet', function () {
        beforeEach(async () => {
            await token.approve(ico.address, toWei(toBN(allowance)));
        });

        it('tracks contributed, raised & forwarded amounts', async function () {
            const walletBalance1 = toBN(await web3.eth.getBalance(wallet));
            console.log(formatNumber(fromWei(walletBalance1)));

            const value1 = toWei("1.0088");
            await ico.buyTokens(investor1, { value: value1, from: investor1 });
            (await ico.cashPayments(investor1)).should.be.bignumber.equal(value1);
            (await ico.cashRaised()).should.be.bignumber.equal(value1);

            await ico.forwardCash();

            await ico.nextStage();

            const value2 = toWei("1.1253");
            await ico.buyTokens(investor2, { value: value2, from: investor2 });
            (await ico.cashPayments(investor2)).should.be.bignumber.equal(value2);
            (await ico.cashRaised()).should.be.bignumber.equal(toBN(value1).add(toBN(value2)));

            const value3 = toWei("0.0353");
            await ico.buyTokens(investor1, { value: value3, from: investor1 });
            (await ico.cashPayments(investor1)).should.be.bignumber.equal(toBN(value1).add(toBN(value3)));
            (await ico.cashRaised()).should.be.bignumber.equal(toBN(value1).add(toBN(value2)).add(toBN(value3)));

            await ico.forwardCash();

            const value4 = toWei("0.0182");
            await ico.buyTokens(investor2, { value: value4, from: investor1 });
            (await ico.cashPayments(investor2)).should.be.bignumber.equal(toBN(value2).add(toBN(value4)));
            (await ico.cashRaised()).should.be.bignumber.equal(toBN(value1).add(toBN(value2)).add(toBN(value3)).add(toBN(value4)));

            await ico.forwardCash();

            const walletBalance2 = toBN(await web3.eth.getBalance(wallet));
            console.log(formatNumber(fromWei(walletBalance2)));
            walletBalance2.sub(walletBalance1).should.be.bignumber.equal(await ico.cashRaised());
        });
    });

    describe('market price fluctuations', function () {
        it('native currency purchasing power follows market price chanages', async function () {

            const value = toWei("1.0000");

            // Initial price and rate
            let price = 120000000;
            let preIcoRate = 2000;
            let converter = new Converter(price, decimals, preIcoRate);

            token = await ExenoToken.new();
            priceFeed = await PriceFeed.new(price, decimals);
            ico = await ExenoTokenIco.new(
                token.address,
                priceFeed.address,
                preIcoRate,
                icoRate,
                minCap,
                maxCap,
                wallet,
                startDate
            );

            const { 0: price1, 1: decimals1 } = await ico.getLatestPrice();
            price1.should.be.bignumber.equal(toBN(price));
            decimals1.should.be.bignumber.equal(toBN(decimals));

            const { tokenAmount: amount1, usdValue: usdValue1 } = converter.convertFromCashAmount(value);
            const { cashAmount: value1, usdValue: usdValueFromTokenAmount1 } = converter.convertFromTokenAmount(amount1);
            const { tokenAmount: tokenAmountFromUSDValue1, cashAmount: cashAmountFromUSDValue1 } = converter.convertFromUSDValue(usdValue1);

            value1.should.be.bignumber.equal(toBN(value));
            usdValueFromTokenAmount1.should.be.bignumber.equal(usdValue1);

            const { 0: _tokenAmountFromCashAmount1, 1: _tokenAmountUSDValue1 } = await ico.convertFromCashAmount(value);
            _tokenAmountFromCashAmount1.should.be.bignumber.equal(amount1);
            _tokenAmountUSDValue1.should.be.bignumber.equal(usdValue1);

            const { 0: _cashAmountFromTokenAmount1, 1: _cashAmountUSDValue1 } = await ico.convertFromTokenAmount(amount1);
            _cashAmountFromTokenAmount1.should.be.bignumber.equal(value);
            _cashAmountUSDValue1.should.be.bignumber.equal(usdValue1);

            const { 0: _tokenAmountFromUSDValue1, 1: _cashAmountFromUSDValue1 } = await ico.convertFromUSDValue(usdValue1);
            _tokenAmountFromUSDValue1.should.be.bignumber.equal(tokenAmountFromUSDValue1);
            _cashAmountFromUSDValue1.should.be.bignumber.equal(cashAmountFromUSDValue1);

            await token.approve(ico.address, toWei(toBN(allowance)));
            const receipt1 = await ico.buyTokens(investor1, { value: value, from: investor1 });
            await expectEvent.inTransaction(receipt1.tx, ico, "TokenPurchase", {
                purchaser: investor1,
                beneficiary: investor1,
                cashAmount: value,
                tokenAmount: amount1
            });
            console.log(formatNumber(fromWei(amount1)));


            //Market price drops by 20%
            price = 96000000;
            converter = new Converter(price, decimals, preIcoRate);

            await priceFeed.updatePrice(price);

            const { 0: price2, 1: decimals2 } = await ico.getLatestPrice();
            price2.should.be.bignumber.equal(toBN(price));
            decimals2.should.be.bignumber.equal(toBN(decimals));

            const { tokenAmount: amount2, usdValue: usdValue2 } = converter.convertFromCashAmount(value);
            const { cashAmount: value2, usdValue: usdValueFromTokenAmount2 } = converter.convertFromTokenAmount(amount2);
            const { tokenAmount: tokenAmountFromUSDValue2, cashAmount: cashAmountFromUSDValue2 } = converter.convertFromUSDValue(usdValue2);

            value2.should.be.bignumber.equal(toBN(value));
            usdValueFromTokenAmount2.should.be.bignumber.equal(usdValue2);

            const { 0: _tokenAmountFromCashAmount2, 1: _tokenAmountUSDValue2 } = await ico.convertFromCashAmount(value);
            _tokenAmountFromCashAmount2.should.be.bignumber.equal(amount2);
            _tokenAmountUSDValue2.should.be.bignumber.equal(usdValue2);

            const { 0: _cashAmountFromTokenAmount2, 1: _cashAmountUSDValue2 } = await ico.convertFromTokenAmount(amount2);
            _cashAmountFromTokenAmount2.should.be.bignumber.equal(value);
            _cashAmountUSDValue2.should.be.bignumber.equal(usdValue2);

            const { 0: _tokenAmountFromUSDValue2, 1: _cashAmountFromUSDValue2 } = await ico.convertFromUSDValue(usdValue2);
            _tokenAmountFromUSDValue2.should.be.bignumber.equal(tokenAmountFromUSDValue2);
            _cashAmountFromUSDValue2.should.be.bignumber.equal(cashAmountFromUSDValue2);

            await token.approve(ico.address, toWei(toBN(allowance)));
            const receipt2 = await ico.buyTokens(investor1, { value: value, from: investor1 });
            await expectEvent.inTransaction(receipt2.tx, ico, "TokenPurchase", {
                purchaser: investor1,
                beneficiary: investor1,
                cashAmount: value,
                tokenAmount: amount2
            });
            console.log(formatNumber(fromWei(amount2)));

            //Purchasing power also drops by 20%
            amount2.should.be.bignumber.equal(amount1.mul(toBN(8)).div(toBN(10)));


            //Market price goes up by 10%
            price = 105600000;
            converter = new Converter(price, decimals, preIcoRate);

            await priceFeed.updatePrice(price);

            const { 0: price3, 1: decimals3 } = await ico.getLatestPrice();
            price3.should.be.bignumber.equal(toBN(price));
            decimals3.should.be.bignumber.equal(toBN(decimals));

            const { tokenAmount: amount3, usdValue: usdValue3 } = converter.convertFromCashAmount(value);
            const { cashAmount: value3, usdValue: usdValueFromTokenAmount3 } = converter.convertFromTokenAmount(amount3);
            const { tokenAmount: tokenAmountFromUSDValue3, cashAmount: cashAmountFromUSDValue3 } = converter.convertFromUSDValue(usdValue3);

            value3.should.be.bignumber.equal(toBN(value));
            usdValueFromTokenAmount3.should.be.bignumber.equal(usdValue3);

            const { 0: _tokenAmountFromCashAmount3, 1: _tokenAmountUSDValue3 } = await ico.convertFromCashAmount(value);
            _tokenAmountFromCashAmount3.should.be.bignumber.equal(amount3);
            _tokenAmountUSDValue3.should.be.bignumber.equal(usdValue3);

            const { 0: _cashAmountFromTokenAmount3, 1: _cashAmountUSDValue3 } = await ico.convertFromTokenAmount(amount3);
            _cashAmountFromTokenAmount3.should.be.bignumber.equal(value);
            _cashAmountUSDValue3.should.be.bignumber.equal(usdValue3);

            const { 0: _tokenAmountFromUSDValue3, 1: _cashAmountFromUSDValue3 } = await ico.convertFromUSDValue(usdValue3);
            _tokenAmountFromUSDValue3.should.be.bignumber.equal(tokenAmountFromUSDValue3);
            _cashAmountFromUSDValue3.should.be.bignumber.equal(cashAmountFromUSDValue3);

            await token.approve(ico.address, toWei(toBN(allowance)));
            const receipt3 = await ico.buyTokens(investor1, { value: value, from: investor1 });
            await expectEvent.inTransaction(receipt3.tx, ico, "TokenPurchase", {
                purchaser: investor1,
                beneficiary: investor1,
                cashAmount: value,
                tokenAmount: amount3
            });
            console.log(formatNumber(fromWei(amount3)));

            //Purchasing power also goes up by 10%
            amount3.should.be.bignumber.equal(amount2.mul(toBN(11)).div(toBN(10)));
        });
    });

    describe('automatic upgrade', function () {
        it('auto-upgrades from preICO to ICO and then to postICO', async function () {
            let price = 1000000;
            let decimals = 0;
            let preIcoRate = 100;
            let icoRate = 200;
            let maxCap = toWei("100000000");

            token = await ExenoToken.new();
            priceFeed = await PriceFeed.new(price, decimals);
            ico = await ExenoTokenIco.new(
                token.address,
                priceFeed.address,
                preIcoRate,
                icoRate,
                minCap,
                maxCap,
                wallet,
                startDate
            );

            await token.approve(ico.address, toWei(toBN(allowance * 2)));
            (await ico.currentStage()).should.be.bignumber.equal(toBN(preIcoStage));

            const value1 = toWei("0.4900");
            await ico.buyTokens(investor1, { value: value1, from: investor1 });

            const tokensPurchased1 = await ico.tokenPurchases(investor1);
            console.log(formatNumber(fromWei(tokensPurchased1)));
            tokensPurchased1.should.be.bignumber.equal(getTokenAmountFromCashAmount(value1, price, decimals, preIcoRate));

            (await ico.currentStage()).should.be.bignumber.equal(toBN(preIcoStage));

            const value2 = toWei("0.0100");
            await ico.buyTokens(investor1, { value: value2, from: investor1 });

            const tokensPurchased2 = await ico.tokenPurchases(investor1);
            console.log(formatNumber(fromWei(tokensPurchased2)));
            tokensPurchased2.should.be.bignumber.equal(tokensPurchased1.add(getTokenAmountFromCashAmount(value2, price, decimals, preIcoRate)));

            (await ico.currentStage()).should.be.bignumber.equal(toBN(icoStage));

            const value3 = toWei("0.49000");
            await ico.buyTokens(investor1, { value: value3, from: investor1 });

            const tokensPurchased3 = await ico.tokenPurchases(investor1);
            console.log(formatNumber(fromWei(tokensPurchased3)));
            tokensPurchased3.should.be.bignumber.equal(tokensPurchased2.add(getTokenAmountFromCashAmount(value3, price, decimals, icoRate)));

            (await ico.currentStage()).should.be.bignumber.equal(toBN(icoStage));

            const value4 = toWei("0.0100");
            await ico.buyTokens(investor2, { value: value4, from: investor2 });

            const tokensPurchased4 = await ico.tokenPurchases(investor2);
            console.log(formatNumber(fromWei(tokensPurchased4)));
            tokensPurchased4.should.be.bignumber.equal(getTokenAmountFromCashAmount(value4, price, decimals, icoRate));

            (await ico.currentStage()).should.be.bignumber.equal(toBN(postIcoStage));
        });
    });
});
