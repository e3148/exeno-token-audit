const { constants, expectRevert } = require("@openzeppelin/test-helpers");
const { toWei, toBN } = require("web3-utils");

require('chai').use(require('chai-as-promised')).should();

const ExenoToken = artifacts.require("ExenoToken");

contract("ExenoToken", async (accounts) => {
    const name = "EXENO COIN";
    const symbol = "EXN";
    const totalSupply = 500 * 1000 * 1000;
    const decimals = 18;

    let token;

    beforeEach(async () => {
        token = await ExenoToken.new();
    });

    describe("token attributes", function () {
        it("has the correct name", async function () {
            (await token.name()).should.equal(name);
        });

        it("has the correct symbol", async function () {
            (await token.symbol()).should.equal(symbol);
        });

        it("has the correct decimals", async function () {
            (await token.decimals()).should.be.bignumber.equal(toBN(decimals));
        });

        it("has the correct total supply", async function () {
            (await token.totalSupply()).should.be.bignumber.equal(toWei(toBN(totalSupply)));
        });
    });

    describe("token actions", function () {
        it("transferring", async () => {
            const initialBalance = await token.balanceOf(accounts[1]);
            const transferAmount = toWei("12.9955");

            await token.transfer(accounts[1], transferAmount, { from: accounts[0] });

            // Ensure balance was properly increased on the receiver's side
            (await token.balanceOf(accounts[1])).should.be.bignumber.equal(initialBalance.add(toBN(transferAmount)));

            const initialBalance1 = await token.balanceOf(accounts[1]);
            const initialBalance2 = await token.balanceOf(accounts[2]);
            const transferAmount2 = toWei("8.15");

            await token.transfer(accounts[2], transferAmount2, { from: accounts[1] });

            // Ensure balances are correct on both accounts
            const newBalance1 = await token.balanceOf(accounts[1]);
            const newBalance2 = await token.balanceOf(accounts[2]);

            newBalance1.should.be.bignumber.equal(initialBalance1.sub(toBN(transferAmount2)));
            newBalance2.should.be.bignumber.equal(initialBalance2.add(toBN(transferAmount2)));

            // Prevent transfering too much
            await expectRevert(
                token.transfer(accounts[2], toWei(newBalance1.add(toBN(toWei("0.01")))), { from: accounts[1] }),
                "ERC20: transfer amount exceeds balance"
            );
        });

        it("allowance", async () => {
            const allowanceAmount = toWei("123.0044");
            await token.approve(accounts[1], allowanceAmount);

            // Ensure allowance amount is correct
            (await token.allowance(accounts[0], accounts[1])).should.be.bignumber.equal(allowanceAmount);

            // Prevent allowance to address 0
            await expectRevert(
                token.approve(constants.ZERO_ADDRESS, allowanceAmount),
                "ERC20: approve to the zero address"
            );
        });

        it("transfering with allowance", async () => {
            const balance = await token.balanceOf(accounts[0]);

            await token.approve(accounts[1], toWei("98.6723"));
            const allowanceAmount = await token.allowance(accounts[0], accounts[1]);

            // Ensure allowance has been properly set
            (await token.allowance(accounts[0], accounts[1])).should.be.bignumber.equal(allowanceAmount);

            // Prevent overspending allowance
            await expectRevert(
                token.transferFrom(accounts[0], accounts[2], allowanceAmount.add(toBN(toWei("0.1"))), { from: accounts[1] }),
                "ERC20: transfer amount exceeds allowance"
            );

            // Prevent overspending balance
            await expectRevert(
                token.transferFrom(accounts[0], accounts[2], balance.add(toBN(toWei("0.1"))), { from: accounts[1] }),
                "ERC20: transfer amount exceeds balance"
            );

            const spendingAmount = toWei("14.5555");
            await token.transferFrom(accounts[0], accounts[2], spendingAmount, { from: accounts[1] });

            // Ensure allowance has been properly updated
            (await token.allowance(accounts[0], accounts[1])).should.be.bignumber.equal(allowanceAmount.sub(toBN(spendingAmount)));

            // Reduce allowance below spendingAmount
            await token.approve(accounts[1], toWei("10.00"));
            await expectRevert(
                token.transferFrom(accounts[0], accounts[2], spendingAmount, { from: accounts[1] }),
                "ERC20: transfer amount exceeds allowance"
            );

            // Increase allowance above spendingAmount
            await token.approve(accounts[1], toWei("15.00"));
            await token.transferFrom(accounts[0], accounts[2], spendingAmount, { from: accounts[1] });
        });
    });
});