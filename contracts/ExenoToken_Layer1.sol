// SPDX-License-Identifier: MIT
pragma solidity ^0.8.4;

import "@openzeppelin/contracts/access/Ownable.sol";
import "erc-payable-token/contracts/token/ERC1363/ERC1363.sol";

contract ExenoToken_Layer1 is
    Ownable,
    ERC1363
{
    string private constant NAME = "EXENO COIN";
    string private constant SYMBOL = "EXN";
    uint256 private constant TOTAL_SUPPLY = 500 * 1000 * 1000 ether;

    constructor()
        ERC20(NAME, SYMBOL)
    {
        _mint(_msgSender(), TOTAL_SUPPLY);
    }

    function burn(uint256 amount)
        external onlyOwner
    {
        _burn(_msgSender(), amount);
    }
}
