// SPDX-License-Identifier: MIT
pragma solidity ^0.8.4;

import "@openzeppelin/contracts/access/AccessControl.sol";
import "@openzeppelin/contracts/access/IAccessControl.sol";
import "erc-payable-token/contracts/token/ERC1363/ERC1363.sol";
import "./maticnetwork/pos-portal/contracts/child/ChildToken/IChildToken.sol";
import "./maticnetwork/pos-portal/contracts/common/AccessControlMixin.sol";
import "./maticnetwork/pos-portal/contracts/common/NativeMetaTransaction.sol";
import "./maticnetwork/pos-portal/contracts/common/ContextMixin.sol";

contract ExenoToken_Layer2 is
    IChildToken,
    AccessControlMixin,
    NativeMetaTransaction,
    ContextMixin,
    ERC1363
{
    string private constant NAME = "EXENO COIN";
    string private constant SYMBOL = "EXN";
    bytes32 public constant DEPOSITOR_ROLE = keccak256("DEPOSITOR_ROLE");

    constructor(
        address childChainManager
    )
        ERC20(NAME, SYMBOL)
    {
        _setupContractId("ExenoToken");
        _setupRole(DEFAULT_ADMIN_ROLE, _msgSender());
        _setupRole(DEPOSITOR_ROLE, childChainManager);
        _initializeEIP712(NAME);
    }

    /**
     * @notice This is to support Native meta transactions
     * @dev Never use `msg.sender` directly, use `_msgSender()` instead
     */
    function _msgSender()
        internal override view returns(address sender)
    {
        return ContextMixin.msgSender();
    }

    /**
     * @notice Called when token is deposited on root chain
     * @dev Should be callable only by ChildChainManager
     * Should handle deposit by minting the required amount for user
     * Make sure minting is done only by this function
     * @param user User address for whom deposit is being done
     * @param depositData ABI-encoded amount
     */
    function deposit(address user, bytes calldata depositData)
        external override only(DEPOSITOR_ROLE)
    {
        uint256 amount = abi.decode(depositData, (uint256));
        _mint(user, amount);
    }

    /**
     * @notice Called when user wants to withdraw tokens back to root chain
     * @dev Should burn user's tokens
     * This transaction will be verified when exiting on root chain
     * @param amount Amount of tokens to withdraw
     */
    function withdraw(uint256 amount) 
        external 
    {
        _burn(_msgSender(), amount);
    }

    function supportsInterface(bytes4 interfaceId)
        public view virtual override(AccessControl,ERC1363) returns(bool)
    {
        return
            interfaceId == type(IAccessControl).interfaceId ||
            interfaceId == type(IERC1363).interfaceId ||
            super.supportsInterface(interfaceId);
    }
}
