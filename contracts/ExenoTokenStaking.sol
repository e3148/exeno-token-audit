// SPDX-License-Identifier: MIT
pragma solidity ^0.8.4;

import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/utils/math/Math.sol";
import "@openzeppelin/contracts/token/ERC20/utils/SafeERC20.sol";
import "@openzeppelin/contracts/security/ReentrancyGuard.sol";
import "@openzeppelin/contracts/security/Pausable.sol";
import "erc-payable-token/contracts/token/ERC1363/IERC1363.sol";
import "erc-payable-token/contracts/payment/ERC1363Payable.sol";

/**
 * @notice Staking utilizing a payable token (ERC1363)
 * No tokens are burnt or minted, whereas staked tokens are stored in this contract
 * Interest payout is done from this contract's owner account (thus prior allowance is needed)
 * Note: Multiple staking contracts can be deployed simultaneously
 */
contract ExenoTokenStaking is
    Ownable,
    Pausable,
    ReentrancyGuard,
    ERC1363Payable
{
    using SafeERC20 for IERC1363;

    /**
     * @notice The tokens being staked
     */
    IERC1363 public immutable token;

    /**
     * @notice Interest rate per annum
     * Expressed as 10**4, e.g. 1285 means 0.1285 or 12.85%
     */
    uint public annualizedInterestRate;

    /**
     * @notice Rebase period, expressed in hours
     * 24 means rebasing every day, 168 means rebasing every week, etc
     */
    uint public immutable rebasePeriodInHours;

    /**
     * @notice Mode of calculating start time for new stakes
     */
    StakeStartMode public stakeStartMode;

    /**
     * @notice Date when staking is terminated
     * Can be extended by calling `extendEndDate()`
     */
    uint public endDate;

    /**
     * @notice The total ammount of currently staked tokens
     */
    uint public totalTokensStaked;

    enum StakeStartMode {
        IMMEDIATE,
        NEAREST_FULL_HOUR,
        NEAREST_MIDNIGHT,
        NEAREST_1AM,
        NEAREST_NOON,
        NEAREST_1PM
    }
    
    /**
     * @dev Represents a single staking deposit
     */
    struct Stake {
        address user;
        uint timestamp;
        uint amount;
        uint interest;
        uint claimable;
        uint nextRebaseClaimable;
        uint nextRebaseTimestamp;
    }

    /**
     * @dev Represents a staker that has active stakes
     */
    struct Stakeholder {
        address user;
        Stake[] stakes;
    }

    /**
     * @dev Represents all stakes performed by a given account
     */
    struct StakingStatus {
        uint timestamp;
        uint totalAmount;
        Stake[] stakes;
    }

    /**
     * @dev Contains all stakes done with this contract
     * The stakes for each address are stored at an index which can be found using the stakes mapping
     */
    Stakeholder[] internal _stakeholders;

    /**
     * @notice Keeps track of the index for the stakers in the stakes array
     */
    mapping(address => uint) internal _stakeMap;

    /**
     * @notice Triggered whenever someone stakes tokens
     * Address is indexed to make it filterable
     */
    event Staked(
        address indexed user,
        uint amount
    );

    /**
     * @notice Triggered whenever someone unstakes tokens
     * Address is indexed to make it filterable
     */
    event Unstaked(
        address indexed user,
        uint amount,
        uint payout
    );

    modifier validAddress(address a) {
        require(a != address(0),
            "ExenoTokenStaking: invalid address");
        require(a != address(this),
            "ExenoTokenStaking: invalid address");
        _;
    }

    constructor(
        IERC1363 token_,
        uint annualizedInterestRate_,
        uint rebasePeriodInHours_,
        uint8 stakeStartMode_,
        uint endDate_
    )
        ERC1363Payable(token_)
    {
        require(annualizedInterestRate_ > 0,
            "ExenoTokenStaking: invalid annualized interest rate");

        require(rebasePeriodInHours_ > 0,
            "ExenoTokenStaking: invalid rebase period");

        require(stakeStartMode_ <= uint(StakeStartMode.NEAREST_1PM),
            "ExenoTokenStaking: invalid stake start mode");

        require(endDate_ > block.timestamp,
            "ExenoTokenStaking: end-date cannot be in the past");

        token = token_;
        annualizedInterestRate = annualizedInterestRate_;
        rebasePeriodInHours = rebasePeriodInHours_;
        stakeStartMode = StakeStartMode(stakeStartMode_);
        endDate = endDate_;
        
        // Index 0 indicates a non-existing stakeholder
        _stakeholders.push();
    }

    /**
     * @dev When tokens are sent to this contract with `transferAndCall` a new stake is created for the sender
     * @param sender The address performing the action
     * @param amount The amount of tokens transferred
     */
    function _transferReceived(address, address sender, uint256 amount, bytes memory)
        internal override
    {
        _stake(sender, amount);
    }

    /**
     * @notice Adds a stakeholder to the _stakeholders array
     */
    function _addStakeholder(address stakeholder)
        internal returns(uint)
    {
        // Push a empty item to make space for the new stakeholder
        _stakeholders.push();
        // Calculate the index of the last item in the array
        uint userIndex = _stakeholders.length - 1;
        // Assign the address to the new index
        _stakeholders[userIndex].user = stakeholder;
        // Add index to the map
        _stakeMap[stakeholder] = userIndex;
        return userIndex;
    }

    /**
     * @dev Creates a new stake for a sender
     * @param stakeholder Who is staking
     * @param amount Amount of tokens to be staked
     */
    function _stake(address stakeholder, uint amount)
        internal whenNotPaused
    {
        require(amount > 0,
            "ExenoTokenStaking: amount cannot be zero");

        require(endDate > block.timestamp,
            "ExenoTokenStaking: end-date is expired");

        // Make sure tokens have been received
        assert(_checkBalanceIntegrity(totalTokensStaked + amount));

        // Mappings in Solidity creates all values, so we can just check the address
        uint userIndex = _stakeMap[stakeholder];
        
        // Check if the staker already has a staked index or if its the first time
        if (userIndex == 0) {
            // This stakeholder stakes for the first time
            // We need to add him to the stakeHolders and also map it into the index of the stakes
            // The index returned will be the index of the stakeholder in the _stakeholders array
            userIndex = _addStakeholder(stakeholder);
        }

        // Use the index to push a new stake
        _stakeholders[userIndex].stakes.push(
            Stake(stakeholder, _calculateStartTimestamp(), amount, 0, 0, 0, 0)
        );

        // Track the balance of staked tokens
        totalTokensStaked += amount;
        assert(_checkBalanceIntegrity(totalTokensStaked));

        // Emit an event that the stake has occured
        emit Staked(stakeholder, amount);
    }

    /**
     * @dev Unstaking logic
     * @param userIndex Who is unstaking
     * @param stakeIndex Index of the stake is the users stake counter, starting at 0 for the first stake
     * @param amount Amount of tokens to be unstaked from stakeIndex
     * @return Amount of tokens to be paid as interest payout
     */
    function _unstake(uint userIndex, uint stakeIndex, uint amount)
        internal returns(uint)
    {
        Stake[] memory stakes = _stakeholders[userIndex].stakes;
        Stake memory currentStake = stakes[stakeIndex];

        // Calculate available reward first before we start modifying data
        (,uint recentRebaseDuration,,) = _calculateDuration(currentStake.timestamp);
        uint payout = _calculatePayout(recentRebaseDuration, amount);
        
        // Remove by subtracting the money unstaked
        currentStake.amount = currentStake.amount - amount;
        
        // If stake is empty, then remove it from the array of stakes
        if (currentStake.amount == 0) {
            delete _stakeholders[userIndex].stakes[stakeIndex];
        } else {
            // If not empty then replace its value
            _stakeholders[userIndex].stakes[stakeIndex].amount = currentStake.amount;
        }

        return payout;
    }

    function _checkBalanceIntegrity(uint expectedBalance)
        internal view returns(bool)
    {
        return token.balanceOf(address(this)) == expectedBalance;
    }

    /**
     * @dev Calculates start time for a new stake
     * @return startTimestamp
     */
    function _calculateStartTimestamp()
        internal view virtual returns(uint)
    {
        if (stakeStartMode == StakeStartMode.IMMEDIATE) {
            return block.timestamp;
        }
        uint startTimestamp = block.timestamp;
        if (stakeStartMode == StakeStartMode.NEAREST_FULL_HOUR) {
            // Reverse time to the top of the hour and then add 1 hour
            startTimestamp = startTimestamp / 1 hours * 1 hours + 1 hours;
        } else if (stakeStartMode == StakeStartMode.NEAREST_MIDNIGHT) {
            // Reverse time to 00:00 UTC and then add 1 day
            startTimestamp = startTimestamp / 1 days * 1 days + 1 days;
        } else if (stakeStartMode == StakeStartMode.NEAREST_1AM) {
            // Reverse time to 00:00 UTC and then add 1 hour
            startTimestamp = startTimestamp / 1 days * 1 days + 1 hours;
        } else if (stakeStartMode == StakeStartMode.NEAREST_NOON) {
            // Reverse time to 00:00 UTC and then add 12 hours
            startTimestamp = startTimestamp / 1 days * 1 days + 12 hours;
        } else if (stakeStartMode == StakeStartMode.NEAREST_1PM) {
            // Reverse time to 00:00 UTC and then add 13 hours
            startTimestamp = startTimestamp / 1 days * 1 days + 13 hours;
        }
        // Shift by 1 day if the new start is earlier than the current time
        if (startTimestamp < block.timestamp) {
            startTimestamp += 1 days;
            assert(startTimestamp >= block.timestamp);
        }
        return startTimestamp;
    }

    /**
     * @dev Calculates durations for different scenarios
     * @param startTimestamp When staking started
     * @return actualDuration Actual duration, expressed in hours
     * @return recentRebaseDuration Recent rebase duration, expressed in hours
     * @return nextRebaseDuration Next rebase duration, expressed in hours
     * @return nextRebaseTimestamp Next rebase timestamp expressed in hours
     */
    function _calculateDuration(uint startTimestamp)
        internal view virtual returns(
            uint actualDuration,
            uint recentRebaseDuration,
            uint nextRebaseDuration,
            uint nextRebaseTimestamp
        )
    {
        // We assume the stake is going to be terminated now
        uint endTimestamp = block.timestamp;
        
        // In case endDate has already happened, allow endTimestamp to extend one extra rebase period
        if (endDate < endTimestamp) {
            endTimestamp = Math.min(endTimestamp, endDate + rebasePeriodInHours * 1 hours);
        }

        // Actual duration of the stake, expressed in hours
        actualDuration = 0;
        
        // When the stake is already started, actualDuration becomes non-zero
        if (startTimestamp < endTimestamp) {
            actualDuration = (endTimestamp - startTimestamp) / 1 hours;
        }

        // Duration of the most recent rebase, expressed in hours
        recentRebaseDuration = (actualDuration / rebasePeriodInHours) * rebasePeriodInHours;

        // If no additional interest can be accrued, reduce actualDuration to recentRebaseDuration
        if (endTimestamp < block.timestamp) {
            actualDuration = recentRebaseDuration;
        }

        // Duration of the next rebase, expressed in hours
        nextRebaseDuration = 0;
        nextRebaseTimestamp = 0;
        
        // If this in not the last rebase, set nextRebaseDuration by extending recentRebaseDuration by one rebase period
        if (startTimestamp + recentRebaseDuration * 1 hours < endDate) {
            nextRebaseDuration = recentRebaseDuration + rebasePeriodInHours;
            nextRebaseTimestamp = startTimestamp + nextRebaseDuration * 1 hours;
        }
    }

    /**
     * @dev Calculates interest payout
     * @param duration Duration expressed in hours
     * @param amount Amount of tokens staked
     * @return Actual or potential payout
     */
    function _calculatePayout(uint duration, uint amount)
        internal view virtual returns(uint)
    {
        return duration * amount * annualizedInterestRate / 365 / 24 / 10**4;
    }

    function pause()
        external onlyOwner
    {
        _pause();
    }

    function unpause()
        external onlyOwner
    {
        _unpause();
    }

    /**
     * @notice Sets a new annualized-interest-rate
     */
    function updateAnnualizedInterestRate(uint newAnnualizedInterestRate)
        external onlyOwner
    {
        require(newAnnualizedInterestRate > 0,
            "ExenoTokenStaking: invalid annualized interest rate");
        
        annualizedInterestRate = newAnnualizedInterestRate;
    }

    /**
     * @notice Sets a new stake-start-mode
     */
    function updateStakeStartMode(uint8 newStakeStartMode)
        external onlyOwner
    {
        require(newStakeStartMode <= uint(StakeStartMode.NEAREST_1PM),
            "ExenoTokenStaking: invalid stake start mode");
        
        stakeStartMode = StakeStartMode(newStakeStartMode);
    }

    /**
     * @notice Sets a new end-date under the following conditions:
     * (1) the existing end-date has not expired yet
     * (2) the new end-date is later then existing end-date
     */
    function extendEndDate(uint newEndDate)
        external onlyOwner
    {
        require(newEndDate > block.timestamp,
            "ExenoTokenStaking: new end-date cannot be in the past");

        require(newEndDate > endDate,
            "ExenoTokenStaking: new end-date cannot be prior to existing end-date");
        
        require(endDate > block.timestamp,
            "ExenoTokenStaking: cannot set a new end-date as existing end-date is expired");
        
        endDate = newEndDate;
    }

    /**
     * @notice Generates a staking status report for a given stakeholder
     * @param stakeholder For whom is the report
     * @return StakingStatus report
     */
    function getStakingStatus(address stakeholder)
        external view validAddress(stakeholder) returns(StakingStatus memory)
    {
        uint totalAmount = 0;
        StakingStatus memory status = StakingStatus(block.timestamp, 0, _stakeholders[_stakeMap[stakeholder]].stakes);
        for (uint i = 0; i < status.stakes.length; i++) {
            uint amount = status.stakes[i].amount;
            if (amount > 0) {
                uint timestamp = status.stakes[i].timestamp;
                (
                    uint actualDuration,
                    uint recentRebaseDuration,
                    uint nextRebaseDuration,
                    uint nextRebaseTimestamp
                ) = _calculateDuration(timestamp);
                status.stakes[i].interest = _calculatePayout(actualDuration, amount);
                status.stakes[i].claimable = _calculatePayout(recentRebaseDuration, amount);
                status.stakes[i].nextRebaseClaimable = _calculatePayout(nextRebaseDuration, amount);
                status.stakes[i].nextRebaseTimestamp = nextRebaseTimestamp;
                totalAmount += amount;
            }
        }
        status.totalAmount = totalAmount;
        return status;
    }

    /**
     * @notice Returns the number of stakeholders
     * @return Number of stakeholders
     */
    function getNumberOfStakeholders()
        external view returns(uint)
    {
        return _stakeholders.length - 1;
    }

    /**
     * @notice Shows the current amount of tokens available for payouts
     * @return Owner's balance and this contract's allowance
     */
    function checkAvailableFunds()
        external view returns(uint256, uint256)
    {
        return (token.balanceOf(owner()), token.allowance(owner(), address(this)));
    }

    /**
     * @notice Withdraws stake from a given staking index and makes appropriate transfers
     * @param amount Amount to unstake, zero indicates maximum available
     * @param stakeIndex Index of the stake is the users stake counter, starting at 0 for the first stake
     */
    function unstake(uint amount, uint stakeIndex)
        external nonReentrant
    {
        address stakeholder = msg.sender;
        uint userIndex = _stakeMap[stakeholder];

        Stake[] memory stakes = _stakeholders[userIndex].stakes;

        require(stakes.length > 0,
            "ExenoTokenStaking: this account has never made any staking");

        require(stakeIndex >= 0 && stakeIndex < stakes.length,
            "ExenoTokenStaking: provided index is out of range");

        Stake memory currentStake = stakes[stakeIndex];

        if (amount > 0) {
            require(currentStake.amount >= amount,
                "ExenoTokenStaking: there is not enough stake on the provided index for the requested amount");
        } else {
            // If amount is zero the entire index will be unstaked
            require(currentStake.amount > 0,
                "ExenoTokenStaking: there is not enough stake on the provided index for the requested amount");
            amount = currentStake.amount;
        }

        uint payout = _unstake(userIndex, stakeIndex, amount);
        
        require(token.balanceOf(owner()) >= payout,
            "ExenoTokenStaking: not enough balance for payout");

        require(token.allowance(owner(), address(this)) >= payout,
            "ExenoTokenStaking: not enough allowance for payout");

        assert(_checkBalanceIntegrity(totalTokensStaked));
        
        assert(totalTokensStaked >= amount);

        // Return staked tokens to the stakeholder
        token.safeTransfer(stakeholder, amount);

        // Send payout to the stakeholder
        if (payout > 0) {
            token.safeTransferFrom(owner(), stakeholder, payout);
        }

        // Track the balance of staked tokens
        totalTokensStaked -= amount;
        
        assert(_checkBalanceIntegrity(totalTokensStaked));

        // Emit an event that unstaking has occured
        emit Unstaked(stakeholder, amount, payout);
    }

    /**
     * @notice Withdraws stake from a given staking index and makes appropriate transfers
     * @notice Does NOT pay the claimable interest!!!
     * @param amount Amount to unstake, zero indicates maximum available
     * @param stakeIndex Index of the stake is the users stake counter, starting at 0 for the first stake
     */
    function unstakeWithoutPayout(uint amount, uint stakeIndex)
        external nonReentrant
    {
        address stakeholder = msg.sender;
        uint userIndex = _stakeMap[stakeholder];

        Stake[] memory stakes = _stakeholders[userIndex].stakes;

        require(stakes.length > 0,
            "ExenoTokenStaking: this account has never made any staking");

        require(stakeIndex >= 0 && stakeIndex < stakes.length,
            "ExenoTokenStaking: provided index is out of range");

        Stake memory currentStake = stakes[stakeIndex];

        if (amount > 0) {
            require(currentStake.amount >= amount,
                "ExenoTokenStaking: there is not enough stake on the provided index for the requested amount");
        } else {
            // If amount is zero the entire index will be unstaked
            require(currentStake.amount > 0,
                "ExenoTokenStaking: there is not enough stake on the provided index for the requested amount");
            amount = currentStake.amount;
        }

        _unstake(userIndex, stakeIndex, amount);

        assert(_checkBalanceIntegrity(totalTokensStaked));

        assert(totalTokensStaked >= amount);

        // Return staked tokens to the stakeholder
        token.safeTransfer(stakeholder, amount);

        // Track the balance of staked tokens
        totalTokensStaked -= amount;

        assert(_checkBalanceIntegrity(totalTokensStaked));

        // Emit an event that unstaking has occured
        emit Unstaked(stakeholder, amount, 0);
    }

    /**
     * @notice Withdraws stake by applying the FIFO rule - no staking index needs to be provided
     * @param amount Amount to unstake, zero indicates maximum available
     */
    function unstakeFifo(uint amount)
        external nonReentrant
    {
        address stakeholder = msg.sender;
        uint userIndex = _stakeMap[stakeholder];

        Stake[] memory stakes = _stakeholders[userIndex].stakes;

        require(stakes.length > 0,
            "ExenoTokenStaking: this account has never made any staking");

        // If amount is zero, all amounts on all indexes will be unstaked
        if (amount == 0) {
            for (uint i = 0; i < stakes.length; i++) {
                amount += stakes[i].amount;
            }
            require(amount > 0,
                "ExenoTokenStaking: not enough stake on all indexes for the requested amount");
        }
        
        // Dry-run to ensure withdraw amount and expected payout can be realized
        uint amountLeft = amount;
        uint expectedPayout = 0;
        uint index = 0;
        while (amountLeft > 0 && index < stakes.length) {
            if (stakes[index].amount > 0) {
                uint bite = Math.min(stakes[index].amount, amountLeft);
                (,uint recentRebaseDuration,,) = _calculateDuration(stakes[index].timestamp);
                expectedPayout += _calculatePayout(recentRebaseDuration, bite);
                amountLeft -= bite;
            }
            index++;
        }

        require(amountLeft == 0,
            "ExenoTokenStaking: not enough stake on all indexes for the requested amount");

        require(token.balanceOf(owner()) >= expectedPayout,
            "ExenoTokenStaking: not enough balance for payout");

        require(token.allowance(owner(), address(this)) >= expectedPayout,
            "ExenoTokenStaking: not enough allowance for payout");

        assert(_checkBalanceIntegrity(totalTokensStaked));

        assert(totalTokensStaked >= amount);
        
        // Actual unstaking loop
        amountLeft = amount;
        uint payout = 0;
        index = 0;
        while (amountLeft > 0 && index < stakes.length) {
            if (stakes[index].amount > 0) {
                uint bite = Math.min(stakes[index].amount, amountLeft);
                payout += _unstake(userIndex, index, bite);
                amountLeft -= bite;
            }
            index++;
        }

        assert(amountLeft == 0);
        assert(payout == expectedPayout);

        // Return staked tokens to the stakeholder
        token.safeTransfer(stakeholder, amount);

        // Send payout to the stakeholder
        token.safeTransferFrom(owner(), stakeholder, payout);

        // Track the balance of staked tokens
        totalTokensStaked -= amount;

        assert(_checkBalanceIntegrity(totalTokensStaked));

        // Emit an event that unstaking has occured
        emit Unstaked(stakeholder, amount, payout);
    }

    /**
     * @notice Withdraws stake by applying the FIFO rule - no staking index needs to be provided
     * @notice Does NOT pay the claimable interest!!!
     * @param amount Amount to unstake, zero indicates maximum available
     */
    function unstakeFifoWithoutPayout(uint amount)
        external nonReentrant
    {
        address stakeholder = msg.sender;
        uint userIndex = _stakeMap[stakeholder];

        Stake[] memory stakes = _stakeholders[userIndex].stakes;

        require(stakes.length > 0,
            "ExenoTokenStaking: this account has never made any staking");

        // If amount is zero, all amounts on all indexes will be unstaked
        if (amount == 0) {
            for (uint i = 0; i < stakes.length; i++) {
                amount += stakes[i].amount;
            }
            require(amount > 0,
                "ExenoTokenStaking: not enough stake on all indexes for the requested amount");
        }
        
        // Dry-run to ensure withdraw amount can be realized
        uint amountLeft = amount;
        uint index = 0;
        while (amountLeft > 0 && index < stakes.length) {
            if (stakes[index].amount > 0) {
                uint bite = Math.min(stakes[index].amount, amountLeft);
                amountLeft -= bite;
            }
            index++;
        }

        require(amountLeft == 0,
            "ExenoTokenStaking: not enough stake on all indexes for the requested amount");

        assert(_checkBalanceIntegrity(totalTokensStaked));

        assert(totalTokensStaked >= amount);
        
        // Actual unstaking loop
        amountLeft = amount;
        index = 0;
        while (amountLeft > 0 && index < stakes.length) {
            if (stakes[index].amount > 0) {
                uint bite = Math.min(stakes[index].amount, amountLeft);
                _unstake(userIndex, index, bite);
                amountLeft -= bite;
            }
            index++;
        }

        assert(amountLeft == 0);

        // Return staked tokens to the stakeholder
        token.safeTransfer(stakeholder, amount);

        // Track the balance of staked tokens
        totalTokensStaked -= amount;

        assert(_checkBalanceIntegrity(totalTokensStaked));

        // Emit an event that unstaking has occured
        emit Unstaked(stakeholder, amount, 0);
    }
}