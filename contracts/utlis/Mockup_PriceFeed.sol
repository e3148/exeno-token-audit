// SPDX-License-Identifier: MIT
pragma solidity ^0.8.4;

import "@chainlink/contracts/src/v0.8/interfaces/AggregatorV3Interface.sol";

contract PriceFeed is AggregatorV3Interface {

    string public override description;
    uint256 public override version;
    uint8 public override decimals;
    int256 private price;

    constructor(int256 _price, uint8 _decimals) {
        description = "";
        version = 0;
        price = _price;
        decimals = _decimals;
    }

    function getRoundData(uint80 _roundId)
        external override view returns(
            uint80 roundId,
            int256 answer,
            uint256 startedAt,
            uint256 updatedAt,
            uint80 answeredInRound
        )
    {
        return (_roundId, price, 0, 0, 0);
    }

    function latestRoundData()
        external override view returns(
            uint80 roundId,
            int256 answer,
            uint256 startedAt,
            uint256 updatedAt,
            uint80 answeredInRound
        )
    {
        return (0, price, 0, 0, 0);
    }

    function updatePrice(int256 _price)
        external
    {
        price = _price;
    }
}
