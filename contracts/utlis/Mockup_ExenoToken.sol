// SPDX-License-Identifier: MIT
pragma solidity ^0.8.4;

import "../ExenoToken_Layer1.sol";
import "../ExenoToken_Layer2.sol";

contract ExenoToken is ExenoToken_Layer1 {}

// contract ExenoToken is ExenoToken_Layer2 {
//     constructor()
//         ExenoToken_Layer2(address(0x0))
//     {
//         _mint(msg.sender, 500 * 1000 * 1000 ether);
//     }
// }
