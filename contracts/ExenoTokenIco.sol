// SPDX-License-Identifier: MIT
pragma solidity ^0.8.4;

import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts/token/ERC20/utils/SafeERC20.sol";
import "@openzeppelin/contracts/security/ReentrancyGuard.sol";
import "@openzeppelin/contracts/security/Pausable.sol";
import "@chainlink/contracts/src/v0.8/interfaces/AggregatorV3Interface.sol";

contract ExenoTokenIco is
    Ownable,
    Pausable,
    ReentrancyGuard
{
    using SafeERC20 for IERC20;

    // Date when ICO starts
    uint256 public immutable startDate;

    // Token being sold
    IERC20 public immutable token;

    // Oracle for native currency market price 
    AggregatorV3Interface public immutable priceFeed;

    // Address where cash is forwarded
    address payable public immutable wallet;

    // How much cash has been raised
    uint256 public cashRaised;
    
    mapping(address => uint256) public tokenPurchases;
    mapping(address => uint256) public cashPayments;
    mapping(uint256 => address) private beneficiaries;

    uint256 public totalTokensPurchased;
    uint256 public totalBeneficiaries;

    enum Stage { PreICO, ICO, PostICO }
    Stage public currentStage;

    // How many US dollars an investor needs to pay for 10**4 tokens, e.g. 3500 means $0.35
    uint256 public preIcoRate;
    uint256 public icoRate;

    // Minimum and maximum amount of tokens an investor is allowed to purchase
    uint256 public immutable minCap;
    uint256 public immutable maxCap;

    // Limits triggering automatic transition to ICO stage and PostICO stage
    uint256 public constant PRE_ICO_LIMIT = 50 * 1000 * 1000 ether;
    uint256 public constant TOTAL_ICO_LIMIT = 75 * 1000 * 1000 ether;
    
    /**
     * Event for token purchase logging
     * @param purchaser who paid for the tokens
     * @param beneficiary who got the tokens
     * @param cashAmount cash paid for the purchase
     * @param tokenAmount amount of tokens purchased
     */
    event TokenPurchase(
        address indexed purchaser,
        address indexed beneficiary,
        uint256 cashAmount,
        uint256 tokenAmount
    );

    modifier validAddress(address a) {
        require(a != address(0),
            "ExenoTokenIco: invalid address");
        require(a != address(this),
            "ExenoTokenIco: invalid address");
        _;
    }

    constructor(
        IERC20 _token,
        AggregatorV3Interface _priceFeed,
        uint256 _preIcoRate,
        uint256 _icoRate,
        uint256 _minCap,
        uint256 _maxCap,
        address payable _wallet,
        uint256 _startDate
    )
        validAddress(_wallet)
    {
        assert(PRE_ICO_LIMIT < TOTAL_ICO_LIMIT);
        
        require(_preIcoRate > 0 && _preIcoRate < _icoRate,
            "ExenoTokenIco: preICO rate needs to be lower than ICO rate");
        
        require(_minCap > 0 && _minCap < _maxCap,
            "ExenoTokenIco: min cap needs to be lower than max cap");
        
        token = _token;
        priceFeed = _priceFeed;
        preIcoRate = _preIcoRate;
        icoRate = _icoRate;
        minCap = _minCap;
        maxCap = _maxCap;
        wallet = _wallet;
        startDate = _startDate;
        currentStage = Stage.PreICO;
    }

    /**
     * @notice Allows investors to purchase tokens
     * @param beneficiary For whom the token is purchased
     */
    function _buyTokens(address beneficiary)
        internal whenNotPaused nonReentrant
    {
        require(block.timestamp >= startDate,
            "ExenoTokenIco: sale has not started yet");
        
        require(currentStage == Stage.PreICO || currentStage == Stage.ICO,
            "ExenoTokenIco: buying tokens is only allowed in preICO and ICO");

        if (currentStage == Stage.PreICO) {
            assert(totalTokensPurchased < PRE_ICO_LIMIT);
        } else if (currentStage == Stage.ICO) {
            assert(totalTokensPurchased < TOTAL_ICO_LIMIT);
        }

        uint256 cashAmount = msg.value;
        require(cashAmount > 0,
            "ExenoTokenIco: invalid value");

        (uint256 tokenAmount,) = convertFromCashAmount(cashAmount);

        require(token.balanceOf(owner()) >= tokenAmount,
            "ExenoTokenIco: not enough balance on owner's account");

        require(token.allowance(owner(), address(this)) >= tokenAmount,
            "ExenoTokenIco: not enough allowance from owner's account");

        uint256 existingPayment = cashPayments[beneficiary];
        uint256 newPayment = existingPayment + cashAmount;

        uint256 existingPurchase = tokenPurchases[beneficiary];
        uint256 newPurchase = existingPurchase + tokenAmount;

        require(newPurchase >= minCap,
            "ExenoTokenIco: purchase is below min cap");

        require(newPurchase <= maxCap,
            "ExenoTokenIco: purchase is above max cap");

        cashRaised += cashAmount;
        totalTokensPurchased += tokenAmount;

        cashPayments[beneficiary] = newPayment;
        tokenPurchases[beneficiary] = newPurchase;

        if (existingPurchase == 0) {
          beneficiaries[totalBeneficiaries] = beneficiary;
          totalBeneficiaries += 1;
        }

        token.safeTransferFrom(owner(), beneficiary, tokenAmount);

        emit TokenPurchase(msg.sender, beneficiary, cashAmount, tokenAmount);

        if (currentStage == Stage.PreICO
        && totalTokensPurchased >= PRE_ICO_LIMIT) {
            currentStage = Stage.ICO;
        } else if (currentStage == Stage.ICO
        && totalTokensPurchased >= TOTAL_ICO_LIMIT) {
            currentStage = Stage.PostICO;
        }
    }

    /**
     * @notice Allows token purchasing via a simple transfer
     */
    receive()
        external payable
    {
        _buyTokens(msg.sender);
    }

    /**
     * @notice External access to `_buyTokens()`
     * @param beneficiary For whom the token is purchased
     */
    function buyTokens(address beneficiary)
        external payable validAddress(beneficiary)
    {
        _buyTokens(beneficiary);
    }

    /**
     * @notice Allows owner to pause further token purchases
     */
    function pause()
        external onlyOwner
    {
        _pause();
    }

    /**
     * @notice Allows owner to unpause further token purchases
     */
    function unpause()
        external onlyOwner
    {
        _unpause();
    }

    /**
     * @notice Allows owner to update the stage
     */
    function nextStage()
        external onlyOwner
    {
        require(currentStage == Stage.PreICO || currentStage == Stage.ICO,
            "ExenoTokenIco: changing stage is only allowed in preICO and ICO");

        if (currentStage == Stage.PreICO) {
            currentStage = Stage.ICO;
        } else if (currentStage == Stage.ICO) {
            currentStage = Stage.PostICO;
        }
    }

    /**
     * @notice Allows owner to update the rates
     * @param newPreIcoRate New preICO rate
     * @param newIcoRate New ICO rate
     */
    function updateRates(uint256 newPreIcoRate, uint256 newIcoRate)
        external onlyOwner
    {
        require(currentStage == Stage.PreICO || currentStage == Stage.ICO,
            "ExenoTokenIco: updating rates is only allowed in preICO and ICO");
        
        require(newPreIcoRate > 0 && newPreIcoRate < newIcoRate,
            "ExenoTokenIco: preICO rate needs to be lower than ICO rate");
        
        preIcoRate = newPreIcoRate;
        icoRate = newIcoRate;
    }

    /**
     * @notice Allows owner to forward cash to the wallet
     */
    function forwardCash()
        external onlyOwner
    {
        uint256 balance = address(this).balance;
        require(balance > 0,
            "ExenoTokenIco: there no cash to forward");
        wallet.transfer(balance);
    }

    /**
     * @notice Figures out the current rate based on the current stage
     * @return the current rate
     */
    function currentRate()
        public view returns(uint256)
    {
        uint256 rate = 0;
        if (currentStage == Stage.PreICO) {
            rate = preIcoRate;
        } else if (currentStage == Stage.ICO) {
            rate = icoRate;
        }
        return rate;
    }

    /**
     * @notice Fetches the most recent market price of the native currency from an external oracle contract
     * @return price The most recent price and its decimals
     * @return decimals Decimals for the price
     */
    function getLatestPrice()
        public view returns(uint256 price, uint8 decimals)
    {
        (, int256 answer, , ,) = priceFeed.latestRoundData();
        price = uint256(answer);
        decimals = priceFeed.decimals();
    }

    /**
     * @notice Shows the current amount of tokens available for sale
     * @return Owner's balance and this contract's allowance
     */
    function checkAvailableFunds()
        external view returns(uint256, uint256)
    {
        return (token.balanceOf(owner()), token.allowance(owner(), address(this)));
    }

    /**
     * @notice Conversion calculation from cash amount (i.e. native currency)
     * @param cashAmount Amount of cash to be converted
     * @return tokenAmount Amount of tokens that can be purchased with the specified cashAmount
     * @return usdValue USD equivalent of cashAmount
     */
    function convertFromCashAmount(uint256 cashAmount)
        public view returns(uint256 tokenAmount, uint256 usdValue)
    {
        (uint256 price, uint8 decimals) = getLatestPrice();
        uint256 rate = currentRate();
        usdValue = cashAmount * price / 10**decimals;
        tokenAmount = usdValue / rate * 10**4;
    }

    /**
     * @notice Conversion calculation from token amount
     * @param tokenAmount Amount of tokens to be converted
     * @return cashAmount Amount of cash (i.e. native currency) needed to purchase the specified tokenAmount
     * @return usdValue USD equivalent of tokenAmount
     */
    function convertFromTokenAmount(uint256 tokenAmount)
        external view returns(uint256 cashAmount, uint256 usdValue)
    {
        (uint256 price, uint8 decimals) = getLatestPrice();
        uint256 rate = currentRate();
        cashAmount = tokenAmount / price * 10**decimals * rate / 10**4;
        usdValue = tokenAmount * rate / 10**4;
    }

    /**
     * @notice Conversion calculation from USD value
     * @param usdValue Amount of USD to be converted
     * @return tokenAmount Amount of tokens that can be purchased with the specified usdValue
     * @return cashAmount Amount of cash (i.e. native currency) equivalent to the specified usdValue
     */
    function convertFromUSDValue(uint256 usdValue)
        external view returns(uint256 tokenAmount, uint256 cashAmount)
    {
        (uint256 price, uint8 decimals) = getLatestPrice();
        uint256 rate = currentRate();
        tokenAmount = usdValue / rate * 10**4;
        cashAmount = usdValue / price * 10**decimals;
    }

}